cmake_minimum_required(VERSION 3.5)
project(following_local_planner)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CUDA_STANDARD 17)

add_compile_options(-Wall -lto -Wextra -Wpedantic -fpermissive  -Wunused-parameter
                    -lpthread -lutil -ldl -Xlinker -lspdlog -export-dynamic)

#find dependencies
find_package(ament_cmake REQUIRED)

#ros deps
find_package(geometry_msgs REQUIRED)
find_package(pluginlib REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(rclcpp REQUIRED)
find_package(ament_cmake REQUIRED)
find_package(builtin_interfaces REQUIRED)
find_package(nav2_common REQUIRED)
find_package(nav2_core REQUIRED)
find_package(nav2_costmap_2d REQUIRED)
find_package(nav2_msgs REQUIRED)
find_package(nav2_util REQUIRED)
find_package(pluginlib REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(rclcpp_lifecycle REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(OpenACC REQUIRED)
#system deps
find_package(Eigen3 REQUIRED)
find_package (Python 3 COMPONENTS Interpreter Compiler Development NumPy)
find_package(Boost 1.71 COMPONENTS math_c99 math_c99f math_c99l math_tr1 math_tr1f math_tr1l REQUIRED)

set(dependencies
        Eigen3
        rclcpp
        nav_msgs
        pluginlib
        geometry_msgs
        builtin_interfaces
        nav2_common
        nav2_core
        nav2_costmap_2d
        nav2_msgs
        nav2_util
        pluginlib
        rclcpp
        rclcpp_action
        rclcpp_lifecycle
        tf2_ros)

if (${Python_NumPy_FOUND})
    include_directories(
                    ${Python_NumPy_INCLUDE_DIRS}
    )
endif()

include_directories(
        ${EIGEN3_INCLUDE_DIRS}
        ${Python_INCLUDE_DIRS}
)

include_directories(include)

## add library lqr_plugins
file(GLOB LQR_SYSTEMS src/lqr_plugins/LQRSystems/*)
add_library(lqr_plugins SHARED src/lqr_plugins/lqr_plugin.cpp src/lqr_plugins/ilqr_plugin.cpp ${LQR_SYSTEMS} src/lqr_plugins/lqr_acc_plugin.cpp tests/matplotlib.hpp)

target_include_directories(lqr_plugins PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)

ament_target_dependencies(
        lqr_plugins

        Eigen3
        rclcpp
        nav_msgs
        pluginlib
        geometry_msgs
)
target_link_libraries(lqr_plugins  ${Eigen3_LIBS} ${OpenACC_LIBRARY})

## Ros navigation plugin
add_library(ros_navigation_plugin SHARED src/ros_plugin/ros_planner.cpp)
target_include_directories(ros_navigation_plugin PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)
ament_target_dependencies(ros_navigation_plugin
        rclcpp_lifecycle
        ${dependencies}
)

## export plugin lib targets
install(FILES plugins/follower_plugins.xml
        DESTINATION share/${PROJECT_NAME})

install(FILES plugins/lqr_system_plugins.xml
        DESTINATION share/${PROJECT_NAME})

install(TARGETS lqr_plugins
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin)

install(TARGETS ros_navigation_plugin
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        RUNTIME DESTINATION bin)

pluginlib_export_plugin_description_file(${PROJECT_NAME} plugins/follower_plugins.xml)
pluginlib_export_plugin_description_file(${PROJECT_NAME} plugins/lqr_system_plugins.xml)
pluginlib_export_plugin_description_file(nav2_core plugins/ros_navigation_plugin.xml)


## testing  section
if (BUILD_TESTING)
    function(add_my_test name filename)
        ament_add_gtest(${name} ${filename})
        ament_target_dependencies(${name}
                ${dependencies}
                )
        target_link_libraries(${name} ros_navigation_plugin lqr_plugins ${Python_LIBRARIES})
    endfunction()
    find_package(ament_lint_auto REQUIRED)
    set(gtest_disable_pthreads OFF)
    ament_lint_auto_find_test_dependencies()
    find_package(ament_cmake_gtest REQUIRED)

    ament_add_gtest(pathBias "tests/functional_test.cpp" )
    ament_target_dependencies(pathBias
            ${dependencies}
    )
    target_link_libraries(pathBias ros_navigation_plugin)

    add_my_test(lqr_speed_test "tests/lqr_speed_test.cpp" )
    add_my_test(ilqr_test tests/ilqr_test.cpp)

endif ()

ament_export_include_directories(include)
ament_export_libraries(lqr_plugins)
ament_export_libraries(ros_navigation_plugin)
ament_export_dependencies(${dependencies})
ament_package()

if(TEST cppcheck)
    # cppcheck test is created in hook in ament_package()
    set_tests_properties(cppcheck PROPERTIES TIMEOUT 400)
endif()

//
// Created by ristle on 1/28/22.
//

#include <bits/stdc++.h>
#include <Eigen/Core>

namespace following_local_planner::functional {
    template<typename T>
    Eigen::MatrixXd dynamicJacobinX(T f, const Eigen::VectorXd &x,
                                    [[maybe_unused]] const Eigen::VectorXd &u) {
        Eigen::MatrixXd A = Eigen::MatrixXd::Zero(x.size(), x.size());
        Eigen::VectorXd yr(x), yl(x);
        double h = 1e-6;

        for (int i = 0; i < A.cols(); ++i) {
            yl(i) -= h;
            yr(i) += h;

            A.col(i) = (f(yr) - f(yl)) / (2 * h);
            yl = x;
            yr = x;
        }

        return A;
    }

    template<typename T>
    Eigen::MatrixXd dynamicJacobinU(T f, const Eigen::VectorXd &x,
                                    const Eigen::VectorXd &u) {
        Eigen::MatrixXd B = Eigen::MatrixXd::Zero(x.size(), u.size());
        double h = 1e-6;

        Eigen::VectorXd yr(u), yl(u);
        for (int i = 0; i < yr.rows(); ++i) {
            yl(i) -= h;
            yr(i) += h;

            B.col(i) = (f(yr) - f(yl)) / (2 * h);
            yl = u;
            yr = u;
        }
        return B;
    }

    template<typename T>
    static inline Eigen::MatrixXd setIdentityMatrix(T &&tupleVariable) {
        ForEachModule fem(std::tuple_size<T>::value);

        for_each_in_tuple(tupleVariable, fem);

        return fem.getMatrix();
    }

    template<typename T>
    T prepareR(const T &speed, const T &t_) {
        return 1.0 /
               (t_ *
                std::pow(speed,
                         2.)); ///< Сообщение для потомков : меньше -> больше выходное
        ///< значение ( Возможно ебучий баг - хуй его знает )
    };

    template<typename T>
    T prepareQ(const T &maxSpeedLocal, const T &t_) {
        return 1.0 / (t_ * std::pow(maxSpeedLocal, 2)); ///< Чем больше, чем важнее
    };

    template<typename T>
    double get_determinant(boost::numeric::ublas::matrix<T> *matrix_boost) {
        auto *result = new double;
#pragma acc enter data copyin(matrix_boost) create(result)
        {
            if (matrix_boost->size1() != matrix_boost->size2()) {
                throw std::runtime_error("Matrix is not quadratic");
            }
            std::size_t dimension = matrix_boost->size1();

            if (dimension == 0) {
                *result = 1;
                return *result;
            }

            if (dimension == 1) {
                *result = (*matrix_boost)(0, 0);
                return *result;
            }

            //Formula for 2x2-matrix
            if (dimension == 2) {
                *result = (*matrix_boost)(0, 0) * (*matrix_boost)(1, 1) - (*matrix_boost)(0, 1) * (*matrix_boost)(1, 0);
                return *result;
            }

            *result = 0;
            int sign = 1;
#pragma acc parallel loop gang worker vector collapse(3)
            for (size_t i = 0; i < dimension; i++) {
                //Submatrix
                boost::numeric::ublas::matrix<T> sub_vector(dimension - 1, dimension - 1);
                for (size_t m = 1; m < dimension; m++) {
                    int z = 0;
                    for (size_t n = 0; n < dimension; n++) {
                        if (n != i) {
                            sub_vector(m - 1, z) = (*matrix_boost)(m, n);
                            z++;
                        }
                    }
                }

                //recursive call
                *result = *result + sign * (*matrix_boost)(0, i) * get_determinant(&sub_vector);
                sign = -sign;
            }
        }
#pragma acc exit copyout(result)
        return *result;
    }


    template<typename T>
    void get_cofactor(boost::numeric::ublas::matrix<T> *input_matrix,
                      boost::numeric::ublas::matrix<T> *output_matrix) {
        if (input_matrix->size1() != input_matrix->size2()) {
            throw std::runtime_error("Matrix is not quadratic");
        }

#pragma acc enter data copyin(input_matrix) create(output_matrix)
        {
            boost::numeric::ublas::matrix<T> sub_matrix(input_matrix->size1() - 1, input_matrix->size2() - 1);

#pragma acc parallel loop gang worker vector collapse(2)
            for (std::size_t i = 0; i < input_matrix->size1(); i++) {
                for (std::size_t j = 0; j < input_matrix->size2(); j++) {

                    int p = 0;
#pragma acc parallel loop worker
                    for (size_t x = 0; x < input_matrix->size1(); x++) {
                        if (x == i) {
                            continue;
                        }
                        int q = 0;

#pragma acc parallel loop vector
                        for (size_t y = 0; y < input_matrix->size1(); y++) {
                            if (y == j) {
                                continue;
                            }
                            sub_matrix(p, q) = (*input_matrix)(x, y);
                            q++;
                        }
                        p++;
                    }
                    (*output_matrix)(i, j) = pow(-1, i + j) * get_determinant(&sub_matrix);
                }
            }
        }
#pragma acc exit copyout(output_matrix)
    }


    template<typename T>
    void get_inverse(boost::numeric::ublas::matrix<T> *input_matrix,
                     boost::numeric::ublas::matrix<T> *output_matrix) {
        auto cofactor_matrix = new boost::numeric::ublas::matrix<T>(input_matrix->size1(), input_matrix->size2());
#pragma acc enter data copyin(input_matrix, cofactor_matrix) create(output_matrix)
        {
            if (get_determinant<T>(input_matrix) == 0) {
                throw std::runtime_error("Determinant is 0");
            }

            double d = 1.0 / get_determinant<T>(input_matrix);

            *output_matrix = *input_matrix;

            get_cofactor(output_matrix, cofactor_matrix);

            *output_matrix = *cofactor_matrix;

            (*output_matrix) *= d;
        }
#pragma acc exit copyout(output_matrix)
    }
} // namespace following_local_planner::functional

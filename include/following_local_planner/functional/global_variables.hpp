//
// Created by ristle on 3/13/22.
//

#pragma  once

#include <bits/stdc++.h>

namespace following_local_planner::global_vars {
    inline double max_speed{0.6};
    inline double average_speed{0.4};
    inline short int frequency{15};
    inline std::mutex frequency_mutex;
    inline std::mutex max_speed_mutex;
    inline std::mutex average_speed_mutex;

    inline decltype(max_speed) getMaxSpeed() {
        std::scoped_lock lock(max_speed_mutex);
        return max_speed;
    }

    inline decltype(average_speed) getAverageSpeed() {
        std::scoped_lock lock(average_speed_mutex);
        return average_speed;
    }

    inline decltype(frequency) getFrequency() {
        std::scoped_lock lock(frequency_mutex);
        return frequency;
    }

    inline void setMaxSpeed(const decltype(average_speed) &new_value) {
        std::scoped_lock lock(max_speed_mutex);
        max_speed = new_value;
    }

    inline void setAverageSpeed(const decltype(average_speed) &new_value) {
        std::scoped_lock lock(average_speed_mutex);
        average_speed = new_value;
    }

    inline void setFrequency(const decltype(frequency) &new_value) {
        std::scoped_lock lock(frequency_mutex);
        frequency = new_value;
    }


};

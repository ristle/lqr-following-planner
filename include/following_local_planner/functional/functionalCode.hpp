//
// Created by ristle on 1/28/22.
//

#pragma once

#include <following_local_planner/functional/global_variables.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <bits/stdc++.h>

#include <Eigen/Core>
#include <Eigen/Dense>

namespace following_local_planner::functional {

/**
 * @brief this function using to calculate Kinematic Matrix dynamically
 * @param f The robot system
 * @param x current state of the robot
 * @param u dynamic (x') in next step
 * @return Dynamic Matrix A
 */
    template<typename T>
    Eigen::MatrixXd dynamicJacobinX(T f, const Eigen::VectorXd &x,
                                    const Eigen::VectorXd &u);

/**
 * @bierf this function using to calculate dynamic Matrix dynamically
 * @param f The robot system
 * @param x current state of the robot
 * @param u dynamic (x') in next step
 * @return Dynamic Matrix B
 */
    template<typename T>
    Eigen::MatrixXd dynamicJacobinU(T f, const Eigen::VectorXd &x,
                                    const Eigen::VectorXd &u);

#ifndef FOR_EACH_IN_TUPLE_H
#define FOR_EACH_IN_TUPLE_H

#include <cstddef>
#include <type_traits>

    template<size_t I = 0, typename Func, typename... Ts>
    typename std::enable_if<I == sizeof...(Ts)>::type
    for_each_in_tuple(std::tuple<Ts...> &, Func &) {}

    template<size_t I = 0, typename Func, typename... Ts>
    typename std::enable_if<
            I < sizeof...(Ts)>::type for_each_in_tuple(std::tuple<Ts...> &tpl,
                                                       Func &func) {
        func(std::get<I>(tpl));
        for_each_in_tuple<I + 1>(tpl, func);
    }

#endif

/**
 * @brief class for for_each iterations for Eigen matrix filling
 * @tparam tuple_ Tuple Type
 */
    class ForEachModule {
    private:
        int index = 0;
        Eigen::MatrixXd matrix_;

    public:
        /**
         * @brief Operator () for  iteratior realization
         * @param T
         */
        template<typename type_>
        void operator()(type_ &T) {
            matrix_(index, index) = T;
            ++index;
        }

        /**
         * @brief Get Filled matrix
         * @return
         */
        Eigen::MatrixXd getMatrix() { return matrix_; }

        /**
         * Simple in-place constructor
         * @param tuple_size_  tuple size
         */
        [[maybe_unused]] ForEachModule(size_t &&tuple_size_) {
            matrix_ = Eigen::MatrixXd::Identity(tuple_size_, tuple_size_);
        }

        /**
         * Simple constructor
         * @param tuple_size_ tuple size
         */
        [[maybe_unused]] ForEachModule(size_t tuple_size_) {
            matrix_ = Eigen::MatrixXd::Identity(tuple_size_, tuple_size_);
        }
    };

/**
 * @brief This function need for Eigen Matrix filling with tuple values
 * @tparam T Tuple of value. Fro example std::tuple<double, double, double,
 * double>>;
 * @param tuple_ The tuple of values std::make_tuple(0.1, 0.2, 0.3, 0.4)
 * @return Filled Identity Matrix
 */
    template<typename T>
    static Eigen::MatrixXd setIdentityMatrix(T &&tupleVariable);

/**
 * @brief Function for preparing R matrix ( R = X/du )
 * @tparam T param for output values
 * @param speed Max speed
 * @param t_ time for stabilization
 * @return
 */
    template<typename T>
    T prepareR(const T &speed, const T &t_ = (1.0));

/**
 * @brief Function for preparing Q matrix ( Q = X/dx )
 * @tparam T param for output values
 * @param speed Max speed
 * @param t_ time for stabilization
 * @return
 */
    template<typename T>
    T prepareQ(const T &maxSpeedLocal, const T &t_ = (1.0));

    template<typename T>
    double get_determinant(boost::numeric::ublas::matrix<T> *matrix_boost);

    template<typename T>
    void get_cofactor(boost::numeric::ublas::matrix<T> *input_matrix, boost::numeric::ublas::matrix<T> *output_matrix);


    template<typename T = double>
    void get_inverse(boost::numeric::ublas::matrix<T> *input_matrix,
                     boost::numeric::ublas::matrix<T> *output_matrix);
} // namespace following_local_planner::functional

#include <following_local_planner/functional/functionalCode.tcc>
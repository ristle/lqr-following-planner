//
// Created by ristle on 1/27/22.
//

#pragma once

#include <bits/stdc++.h>
#include <following_local_planner/core/State.hpp>
#include <following_local_planner/functional/functionalCode.hpp>
#include <following_local_planner/functional/global_variables.hpp>
#include <tf2/LinearMath/Matrix3x3.h>

#include <Eigen/Core>
#include <nav_msgs/msg/path.hpp>

namespace following_local_planner::core {

/**
 * this abstract Class created for different dynamic model initializing
 */
    template<typename T>
    class Model : public std::enable_shared_from_this<Model<T>> {
    protected:
        nav_msgs::msg::Path *trajectory, *trajectorySpeed, *trajectoryAcc;

        int index{0};
        int frequency{30}; ///< Init it from file automotive
        Eigen::VectorXd system_error_;
        Eigen::VectorXd current_state = Eigen::VectorXd(3);
        Eigen::VectorXd desire_state;

        Model() = default;

        Model(nav_msgs::msg::Path *trajectory_,
              nav_msgs::msg::Path *trajectory_speed_,
              nav_msgs::msg::Path *trajectory_acc_)
                : trajectory(trajectory_), trajectorySpeed(trajectory_speed_),
                  trajectoryAcc(trajectory_acc_) {};

    public:
        virtual ~Model() = default;

        /**
         * Get new state for A and B Matrices
         * @param index index of desire elements
         * @return New accelerations
         */
        virtual Eigen::VectorXd getNextDesireOutputControls(unsigned int index) = 0;

        /**
         * Get Q and R Matrices for our Dynamic system based on current Max Speed
         * @param max_speed Current max speed
         * @return tuple of new identity Matrices
         */
        virtual std::tuple<Eigen::MatrixXd, Eigen::MatrixXd>
        getWeightMatrices(double &max_speed, unsigned int index) = 0;

        /**
         * @brief Derivative
         * For example, model of dynamic for multi-directional robot
         *  newState(2) = u(1); ///< Theta
         *  newState(0) = u(0) * std::cos(newState(2)); ///< X
         *  newState(1) = u(0) * std::sin(newState(2)); ///< Y
         *
         * @param x State
         * @param u Velocities
         * @return new pose
         */
        virtual Eigen::VectorXd dynamics(const Eigen::VectorXd &x,
                                         const Eigen::VectorXd &u, double dt) = 0;

        /**
         * @brief Function used for Error calculation for Trajectory Following Problem
         * @param state Current state of the robot
         * @param tf Trajectory Follower class which used for getting desire state
         * @param index Index of the desire state which can be gotten from tf params
         * @return Current Error and desire state
         */
        virtual std::tuple<Eigen::VectorXd, Eigen::VectorXd>
        calculateError(State <T> *state, int &index) = 0;

        virtual void setState(State <T> *state) {
            current_state << state->x, state->y, state->yaw;
        }

        virtual Eigen::VectorXd additional(Eigen::VectorXd &uStar,
                                           [[maybe_unused]] State <T> *stateLocal) {
            return uStar;
        }

        /**
         * @brief Created for Dynamic model implementation. Needed for A and B matrix
         * calculation
         * @param x Current of the robot
         * @param u Dynamic changing for robot
         * @param dt 1 / frequency
         * @return Changing for new pose. Delta of pose)
         */
        Eigen::VectorXd integrateDynamics(Eigen::VectorXd x, Eigen::VectorXd u,
                                          double dt) {
            return x + dynamics(x, u, dt);
        }

        [[maybe_unused]] std::tuple<T, T, T> getPoint(const nav_msgs::msg::Path *path,
                                                      const int &index) {
            double r, p, y;
            tf2::Quaternion quaternion_tf(path->poses[index].pose.orientation.x,
                                          path->poses[index].pose.orientation.y,
                                          path->poses[index].pose.orientation.z,
                                          path->poses[index].pose.orientation.w);

            tf2::Matrix3x3 rotation_matrix(quaternion_tf);
            rotation_matrix.getRPY(r, p, y);

            return std::make_tuple(path->poses[index].pose.position.x,
                                   path->poses[index].pose.position.y, y);
        }

        std::tuple<Eigen::VectorXd, Eigen::VectorXd> getStates() {
            return std::make_tuple(system_error_, desire_state);
        }

        /**
         * This function is used to prevent dynamic calculation, which can destroy
         * some Correlation's
         * @return
         */
        virtual std::tuple<Eigen::MatrixXd, Eigen::MatrixXd>
        getDynamicJacobianMatrices() {
            Eigen::VectorXd c, x_, u;
            auto dt = 1. / this->frequency;

            this->index = getNextIndex(index);
            auto template_state = new State<T>();
            *template_state = current_state;
            try {
                if (this->index >= (int) this->trajectory->poses.size()) {
                    this->index = this->trajectory->poses.size() - 1;
                }
                u = getNextDesireOutputControls(this->index);
                std::tie(c, x_) = calculateError(template_state, this->index);

            } catch (...) {
                u = Eigen::VectorXd::Zero(2);
                x_ = Eigen::VectorXd::Ones(3);
            }

            std::function<Eigen::VectorXd(Eigen::VectorXd)> dyn_x =
                    std::bind(&following_local_planner::core::Model<T>::integrateDynamics,
                              this, std::placeholders::_1, u, dt);
            std::function<Eigen::VectorXd(Eigen::VectorXd)> dyn_u =
                    std::bind(&following_local_planner::core::Model<T>::integrateDynamics,
                              this, x_, std::placeholders::_1, dt);

            Eigen::MatrixXd A = functional::dynamicJacobinX(dyn_x, x_, u);
            Eigen::MatrixXd B = functional::dynamicJacobinU(dyn_u, x_, u);

            return std::make_tuple(A, B);
        };

        inline void setTrajectory(nav_msgs::msg::Path *trajectory_,
                                  nav_msgs::msg::Path *trajectory_speed_) {
            trajectory = trajectory_;
            trajectorySpeed = trajectory_speed_;
        }

        inline void setAcceleration(nav_msgs::msg::Path *trajectory_acc_) {
            trajectoryAcc = trajectory_acc_;
        }

        inline void setStates(nav_msgs::msg::Path *trajectory_,
                              nav_msgs::msg::Path *trajectory_speed_,
                              nav_msgs::msg::Path *trajectory_acc_) {
            trajectory = trajectory_;
            trajectorySpeed = trajectory_speed_;
            trajectoryAcc = trajectory_acc_;
        }

        inline Eigen::VectorXd getCurrentState() { return this->current_state; }

        virtual int getNextIndex(int &indexLocal) {
            return indexLocal + this->frequency * 0.25;
        }
    };

} // namespace following_local_planner::core

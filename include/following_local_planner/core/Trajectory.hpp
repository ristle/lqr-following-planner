//
// Created by ristle on 3/12/22.
//

#pragma once

#include <bits/stdc++.h>
#include <following_local_planner/core/State.hpp>
#include <following_local_planner/functional/global_variables.hpp>

#include <geometry_msgs/msg/twist_stamped.hpp>
#include <nav_msgs/msg/path.hpp>
#include <rclcpp/rclcpp.hpp>

namespace following_local_planner::core {
    class Trajectory {
    public:
        std::vector<double> velocities;

        ~Trajectory() = default;

        [[maybe_unused]] std::tuple<nav_msgs::msg::Path *, nav_msgs::msg::Path *> getTrajectories() {
            return std::make_tuple(&trajectory, &trajectory_speed);
        }


        virtual nav_msgs::msg::Path *getTrajectory() {
            return &trajectory;
        }

    protected:
        double *cost_map;
        double time{}, frequency, path_delta{}, max_velocity, trajectory_time{}, average_velocity, current_max_speed{};
        State<double> *state{};
        nav_msgs::msg::Path trajectory;
        nav_msgs::msg::Path trajectory_acc;
        nav_msgs::msg::Path trajectory_speed;


        Trajectory(int frequency_, double max_velocity_, double average_velocity_, double *cost_map_) :
                cost_map(cost_map_), frequency(frequency_), max_velocity(max_velocity_),
                average_velocity(average_velocity_) {};

        Trajectory(int frequency_, double max_velocity_, double average_velocity_) : cost_map(nullptr),
                                                                                     frequency(frequency_),
                                                                                     max_velocity(max_velocity_),
                                                                                     average_velocity(
                                                                                             average_velocity_) {};

        Trajectory(int *frequency_, double *max_velocity_, double *average_velocity_) : cost_map(nullptr),
                                                                                        frequency(*frequency_),
                                                                                        max_velocity(*max_velocity_),
                                                                                        average_velocity(
                                                                                                *average_velocity_) {};

    public:

        /**
         * @brief loop function for your algorithm
         * @param path global path
         * @param Current Current State
         * @param Output Output State
         * @param status current status of the robot
         */
        virtual void createTrajectory(nav_msgs::msg::Path &path) = 0;

        void initialize(int frequency_, double max_velocity_, double average_velocity_, double *cost_map_) {
            cost_map = cost_map_;
            frequency = frequency_;
            max_velocity = max_velocity_;
            average_velocity = average_velocity_;
        }

        /**
         * @brief Get time for all trajectory
         * @return time
         */
        inline double getTrajectoryTime() { return trajectory_time; }

        /**
         * @brief Get velocities from SQP
         * @return
         */
        inline std::vector<double> *getVelocities() { return &velocities; }

        inline void setAverageVelocity(const double &vel) { average_velocity = vel; }

        [[maybe_unused]]  inline double getAverageVelocity() { return average_velocity; }

        inline void setPathDelta(const double &delta) { path_delta = delta; }

        [[maybe_unused]]  inline double getPathDelta() { return path_delta; }

        inline void setCurrentMaxSpeed(const double &max_speed) { current_max_speed = max_speed; }

        inline double getCurrentMaxSpeed() { return current_max_speed; }

        inline void setMaxVelocity(const double &max_speed) { max_velocity = max_speed; }

        [[maybe_unused]]  inline double getMaxVelocity() { return max_velocity; }

        /**
         * @brief Set current state of the robot
         * @param state_
         */
        inline void setState(State<double> *state_) { state = state_; };

        std::tuple<nav_msgs::msg::Path *, nav_msgs::msg::Path *, nav_msgs::msg::Path *>
        getTrajectoriesWithAcceleration() {
            return std::make_tuple(&trajectory, &trajectory_speed, &trajectory_acc);
        }
    };
}

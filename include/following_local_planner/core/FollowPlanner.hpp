//
// Created by ristle on 1/24/22.
//

#pragma once

#include <bits/stdc++.h>
#include <following_local_planner/core/State.hpp>
#include <following_local_planner/functional/global_variables.hpp>

#include <geometry_msgs/msg/twist_stamped.hpp>
#include <nav_msgs/msg/path.hpp>
#include <rclcpp/rclcpp.hpp>

namespace following_local_planner::core {

class Follower {
public:
  Follower() = default;

  ~Follower() = default;

  /**
   * @brief This is simple subscriber to global plan
   * @param path input global path
   */
  virtual void setPlan(const nav_msgs::msg::Path &path) { global_path = path; }

  /**
   *
   * @param global_path This is path which we follow
   * @param cmd_vel This is output speed
   * @return Getting velocity based on following path
   */
  virtual bool createVelocities(const ::nav_msgs::msg::Path &global_path,
                                core::State<double> *current_state,
                                ::geometry_msgs::msg::Twist &cmd_vel) {
    this->global_path = global_path;

    auto cmd_vel_eigen = this->getSpeed(current_state);

    cmd_vel.linear.x = cmd_vel_eigen(0);  ///< Set Linear speed
    cmd_vel.angular.z = cmd_vel_eigen(1); ///< Set Angualr speed

    return true;
  };

/**
 * @fn std:s:tuple<double, int, double, double, double>
 * LQR_speed_steering_control(State<double> *state, std::vector<State<double>>
 * *st, double &pe, double &pth_e, std::vector<double> &sp)
 * @brief Main loop for LQR with speed control
 *
 * @param state Current state of the robot
 * @param st  All states of the trajectory
 * @param pe  deviation from trajectory
 * @param pth_e deviation from desire angle velocity
 * @param sp speed profile for all poses of the trajectory
 * @return
 */
virtual Eigen::VectorXd getSpeed(core::State<double> *state) = 0;

protected:
  /// Global plan from another part of the planning module
  nav_msgs::msg::Path *trajectory, *trajectory_speed, *trajectory_acc;
  nav_msgs::msg::Path global_path;
  double average_velocity{0.4};
  double max_speed{0.5};
  int frequency{30};

    /**
     * @brief You simple override this function and do what ever you want
     * @param cmd_vel Input velocities
     * @return Corrected velocities
     */
  virtual ::geometry_msgs::msg::Twist
  additional(::geometry_msgs::msg::Twist &cmd_vel) {
    return cmd_vel;
  }

  [[maybe_unused]] static double normalizeAngle(double &x, double &y) {
    double wAngle = std::atan(fabs(y / x));

    wAngle = y > 1e-16 && x < -1e-16 ? M_PI - wAngle : wAngle;
    wAngle = y < -1e-16 && x > 1e-16 ? 2 * M_PI - wAngle : wAngle;
    wAngle = y < -1e-16 && x < -1e-16 ? wAngle + M_PI : wAngle;

    wAngle = fabs(y) <= 1e-16 && x > 1e-16 ? 0 : wAngle;
    wAngle = fabs(x) <= 1e-16 && y > 1e-16 ? 1.57 : wAngle;
    wAngle = fabs(y) <= 1e-16 && x < -1e-16 ? M_PI : wAngle;
    wAngle = fabs(x) <= 1e-16 && y < -1e-16 ? 4.71 : wAngle;

    return wAngle;
  }

  /**
   * this function is used for checking whole trajectory on obstacles
   * @tparam T current state param
   * @param pos current state in State<T> class
   * @param vel new velocities
   * @param baseTrajectory whole trajectory
   * @param costMapRosLocal fucking cost map 2d
   * @return can we follow or not based on obstacles
   */
  bool
        CheckRotateTrajectory([[maybe_unused]] core::State<double> *pos, [[maybe_unused]] geometry_msgs::msg::Twist vel,
                              [[maybe_unused]] nav_msgs::msg::Path *baseTrajectoryLocal/*,
                                   costmap_2d::Costmap2DROS *costMapRosLocal*/)
  {
    // TODO: add map checking
    return true;
  };
};
} // namespace following_local_planner::core

//
// Created by ristle on 1/27/22.
//

#pragma once

#include <Eigen/Core>
#include <Eigen/Dense>

namespace following_local_planner::core {
/*! \struct State
 *  \brief       Make statement of the system in one place
 *              Use templates to use States in different math systems
 *  \param x OX position
 *  \param y OY position
 *  \param yaw 2D angle
 *  \param velocity current speed
 *  \param curvature curvature of the polinomic function in nearest position
 */
template <typename State_T> class State {
public:
  State_T x, y, yaw, velocity, yaw_speed, x_speed, y_speed, x_acc, y_acc,
      yaw_acc;

  State(State_T default_value = 0)
      : x(default_value), y(default_value), yaw(default_value),
        velocity(default_value), yaw_speed(default_value), x_acc(default_value),
        y_acc(default_value), yaw_acc(default_value) {
  } /**< DefaulState_T constructor for simple declaration  */

  State(State_T x_, State_T y_, State_T default_value = 0)
      : x(x_), y(y_), yaw(default_value), velocity(default_value),
        yaw_speed(default_value), x_acc(default_value), y_acc(default_value),
        yaw_acc(default_value) {} /**< Advanced declaration for redeclaration */

  State(State_T x_, State_T y_, State_T yaw_, State_T default_value = 0)
      : x(x_), y(y_), yaw(yaw_), velocity(default_value),
        yaw_speed(default_value), x_acc(default_value), y_acc(default_value),
        yaw_acc(default_value) {} /**< Advanced declaration for redeclaration */

  State(State_T *state, State_T vel, State_T default_value = 0)
      : x(state[default_value]), y(state[1]), yaw(state[2]), velocity(vel),
        yaw_speed(default_value), x_acc(default_value), y_acc(default_value),
        yaw_acc(default_value) {} /**< Advanced declaration for redeclaration */

  State(State_T x_, State_T y_, State_T yaw_, double velocity_,
        State_T default_value = 0)
      : x(x_), y(y_), yaw(yaw_), velocity(velocity_), yaw_speed(default_value),
        x_acc(default_value), y_acc(default_value), yaw_acc(default_value) {
  } /**< Advanced declaration for redeclaration */

  State(State_T x_, State_T y_, State_T yaw_, State_T x_speed_,
        State_T y_speed_, State_T x_acc_, State_T y_acc_,
        State_T default_value = 0)
      : x(x_), y(y_), yaw(yaw_), velocity(std::hypotf(x_speed_, y_speed_)),
        yaw_speed(default_value), x_speed(x_speed_), y_speed(y_speed_),
        x_acc(x_acc_), y_acc(y_acc_), yaw_acc(default_value) {
  } /**< Advanced declaration for redeclaration */

  State(const Eigen::VectorXd &X) {
    x = X(0);
    y = X(1);
    yaw = X(2);

    if (X.size() > 3) {
      yaw_speed = X(4);
      velocity = X(3);
    }
  }

  State<State_T> &operator=(const State<State_T> &state) {
    x = state.x;
    y = state.y;
    yaw = state.yaw;

    x_speed = state.x_speed;
    y_speed = state.y_speed;
    yaw_speed = state.yaw_speed;

    x_acc = state.x_acc;
    y_acc = state.y_acc;
    yaw_acc = state.yaw_acc;

    velocity = state.velocity;

    return *this;
  }

  State<State_T> &operator=(const Eigen::VectorXd &X) {
    x = X(0);
    y = X(1);
    yaw = X(2);

    if (X.size() > 3) {
      yaw_speed = X(4);
      velocity = X(3);
    }
    return *this;
  }

  State<State_T> &operator=(State<State_T> *state) {
    x = state->x;
    y = state->y;
    yaw = state->yaw;

    x_speed = state->x_speed;
    y_speed = state->y_speed;
    yaw_speed = state->yaw_speed;

    x_acc = state->x_acc;
    y_acc = state->y_acc;
    yaw_acc = state->yaw_acc;

    velocity = state->velocity;

    return *this;
  }
};
}; // namespace following_local_planner::core

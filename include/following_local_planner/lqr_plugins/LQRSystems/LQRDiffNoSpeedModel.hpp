//
// Created by ristle on 1/27/22.
//

#pragma once

#include <bits/stdc++.h>
#include <following_local_planner/core/State.hpp>
#include <following_local_planner/core/LQRFollowingSystem.hpp>
#include <following_local_planner/functional/global_variables.hpp>

#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <pluginlib/class_list_macros.hpp>

namespace following_local_planner::plugins {
/**
 * This class provide Dynamic Model implementation for LQR of 3 state
 * controlling: (X, Y, Theta)
 */
template <typename T>
class DiffNoSpeedModel : public core::Model<T>,
                         std::enable_shared_from_this<DiffNoSpeedModel<T>> {
private:
  std::tuple<Eigen::VectorXd, Eigen::VectorXd>
  calculateErrorInternal(core::State<T> *state, int &index);

  inline std::tuple<Eigen::VectorXd, Eigen::VectorXd>
  calculateErrorInternal(core::State<T> *state, int &&index) {
    current_state = state;
    return calculateErrorInternal(state, index);
  }

  T MAX_GLOBAL_SPEED{1.0};
  core::State<T> *current_state{nullptr};
  Eigen::VectorXd xValues = Eigen::VectorXd(6);
  Eigen::VectorXd yValues = Eigen::VectorXd(6);

public:
  /** Only for standard init **/
  DiffNoSpeedModel(nav_msgs::msg::Path *trajectory_,
                   nav_msgs::msg::Path *trajectory_speed_,
                   nav_msgs::msg::Path *trajectory_acc_);

  DiffNoSpeedModel() : core::Model<T>(){};

  ~DiffNoSpeedModel() = default;

  /**
   * Get new state for A and B Matrices
   * @param index index of desire element
   * @return New accelerations
   */
  Eigen::VectorXd getNextDesireOutputControls(unsigned int index) override;

  /**
   * Get Q and R Matrices for our Dynamic system based on current Max Speed
   * @param max_speed Current max speed
   * @return tuple of new identity Matrices
   */
  std::tuple<Eigen::MatrixXd, Eigen::MatrixXd>
  getWeightMatrices(double &max_speed, unsigned int index) override;

  /**
   * Getting increment or decrement of dynamic model for current state and
   * velocity
   * @param x Current State
   * @param u New velocity
   * @return increment/decrement for current state
   */
  Eigen::VectorXd dynamics(const Eigen::VectorXd &x, const Eigen::VectorXd &u,
                           double dt) override;

  /**
   * Error calculation for trajectory following problem.
   * @param state Current State of the robot using State structure
   * @param index Index of the element. Trying to use already exist pointer
   * @return Error, current state in Eigen Format
   */
  std::tuple<Eigen::VectorXd, Eigen::VectorXd>
  calculateError(core::State<T> *state, int &index) override;

  int getNextIndex(int &indexLocal) override;

  Eigen::VectorXd additional(Eigen::VectorXd &uStar,
                             core::State<T> *stateLocal) override;
};
}; // namespace following_local_planner::plugins
//
// Created by ristle on 3/12/22.
//

#pragma once

//
// Created by ristle on 13.07.2021.
//

#pragma once

#include <following_local_planner/core/State.hpp>
#include <following_local_planner/core/Trajectory.hpp>
#include <following_local_planner/lqr_plugins/lqr_planner.hpp>
#include <following_local_planner/functional/global_variables.hpp>

#include <vector>
#include <cmath>
#include <limits>
#include <iostream>
#include <tf2/utils.h>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

namespace following_local_planner::plugins {

    static const int X_DIM = 3;
    static const int U_DIM = 2;
    static const int DIM = 2;

    class iLQR : public core::Trajectory,
                 public std::enable_shared_from_this<iLQR> {
    private:
        template<int Dim>
        using Vector = Eigen::Matrix<double, Dim, 1>;

        template<int Dim>
        using SymmetricMatrix = Eigen::Matrix<double, Dim, Dim>;

        template<int rDim, int cDim>
        using Matrix = Eigen::Matrix<double, rDim, cDim>;
    public:
        explicit iLQR(int frequency_ = 30,
                      double max_velocity_ = 0.6,
                      double average_velocity_ = 0.4, double *cost_map_ = nullptr,
                      const std::string &modelTypeLocal = "DiffNoSpeedModel");

        iLQR(int frequency_,
             double max_velocity_,
             double average_velocity_);

        iLQR(int *frequency_,
             double *max_velocity_,
             double *average_velocity_);

        void createTrajectory(nav_msgs::msg::Path &path) override;

        [[maybe_unused]]  nav_msgs::msg::Path *getTrajectory() {
            return &this->trajectory;
        }

        ~iLQR() = default;

    private:
        double dt = 1. / global_vars::getFrequency();
        int iter{};
        SymmetricMatrix<U_DIM> R;
        SymmetricMatrix<X_DIM> Q;
        Vector<X_DIM> xGoal, xStart;
        std::vector<Vector<U_DIM> > l;
        std::vector<Matrix<U_DIM, X_DIM> > L;

        Vector<U_DIM> uNominal = Vector<U_DIM>::Zero();

        std::shared_ptr<core::Model<double>> model;

        void quadratizeObstacleCost(const Vector<X_DIM> &x, SymmetricMatrix<X_DIM> &Q, Vector<X_DIM> &q);


        void regularize(SymmetricMatrix<DIM> &Q);

        // local cost function c_t(x_t, u_t)
        double localCost(const Vector<X_DIM> &x, const Vector<U_DIM> &u, const int &t);

        void quadratizeCost(const Vector<X_DIM> &x,
                            const Vector<U_DIM> &u,
                            const int &t,
                            Matrix<U_DIM, X_DIM> &Pt,
                            SymmetricMatrix<X_DIM> &Qt,
                            SymmetricMatrix<U_DIM> &Rt,
                            Vector<X_DIM> &qt,
                            Vector<U_DIM> &rt,
                            const int &iter);

        // final cost function
        double cell(const Vector<X_DIM> &x);

        void quadratizeFinalCost(const Vector<X_DIM> &x, SymmetricMatrix<X_DIM> &Qell,
                                 Vector<X_DIM> &qell, const int &iter);

        // Discrete-time dynamics x_{t+1} = g(x_t, u_t)
        [[maybe_unused]] Vector<X_DIM> getNewPose(const Vector<X_DIM> &x, const Vector<U_DIM> &u);
    };
}

//
// Created by ristle on 1/24/22.
//

#pragma once

#include <bits/stdc++.h>

#include <following_local_planner/core/FollowPlanner.hpp>
#include <following_local_planner/core/LQRFollowingSystem.hpp>
#include <following_local_planner/functional/global_variables.hpp>
#include <following_local_planner/lqr_plugins/LQRSystems/LQRDiffNoSpeedModel.hpp>

#include <omp.h>
#include <tf2/LinearMath/Matrix3x3.h>

#include <typeinfo>
#include <Eigen/QR>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <rclcpp/rclcpp.hpp>
#include <pluginlib/class_loader.hpp>
#include <pluginlib/class_list_macros.hpp>

namespace following_local_planner::plugins {

template <typename T>
class LQR : public following_local_planner::core::Follower,
            public std::enable_shared_from_this<LQR<T>> {
public:
  Eigen::MatrixXd kCalculated;
  Eigen::MatrixXd K;

  typedef Eigen::MatrixXd MatrixXd;
  typedef Eigen::VectorXd VectorXd;

private:
  double min_ = std::numeric_limits<double>::max(),
         dt = 1. / this->frequency; ///< wheel base of the vehicle [m]
  bool calculateLQR{true};
  VectorXd desire_state, system_error_;

public:
  /**
   * A constructor
   * Simple constructor
   */
  LQR(double max_velocity,
      following_local_planner::core::State<double> *stateLocal,
      T *costMap_ = nullptr);

  LQR() : following_local_planner::core::Follower() {
    model.reset(new DiffNoSpeedModel<double>(
        this->trajectory, this->trajectory_speed, this->trajectory_acc));
    //            resetModel(ModelType::DiffNoSpeed);
  }

  /**
   * @fn Eigen::MatrixXd solve_dare(Eigen::MatrixXd *A, Eigen::MatrixXd *B,
   * Eigen::MatrixXd *Q, Eigen::MatrixXd *R)
   * @brief Try to solve equation simpler method
   *
   * @param A  Kinematic matrix (5, 3)
   * @param B  Dynamic Vector (5)
   * @param Q  Identity Matrix for advanced  equations (5, 3)
   * @param R  Identity Matrix for advanced  equations (2, 2)
   * @return   K matrix -> solution for u = K * x;
   */
  Eigen::MatrixXd solveDare(Eigen::MatrixXd *A, Eigen::MatrixXd *B,
                            Eigen::MatrixXd *Q, Eigen::MatrixXd *R);

  /**
   * @fn Eigen::Matrix<double, 2, 5> DLQR(Eigen::MatrixXd *A, Eigen::MatrixXd
   * *B, Eigen::MatrixXd *Q, Eigen::MatrixXd *R)
   * @brief Find a solution for linear quadratic equations. Here comes new
   * solution for acceleration calculation based on current speed of the robot
   *
   * @param A  Kinematic matrix (5, 3)
   * @param B  Dynamic Vector (5)
   * @param Q  Identity Matrix for advanced  equations (5, 3)
   * @param R  Identity Matrix for advanced  equations (2, 2)
   * @return   K matrix -> solution for u = K * x;
   */
  Eigen::MatrixXd DLQR(Eigen::MatrixXd *A, Eigen::MatrixXd *B,
                       Eigen::MatrixXd *Q, Eigen::MatrixXd *R);

  /**
   * Set dynamic model
   * @param model_  abstract class for different models;
   */
  [[maybe_unused]] inline void setModel(core::Model<double> *modelLocal) {
    model.reset(modelLocal);
  }

  void setPlan(const nav_msgs::msg::Path &path) override {

    this->global_path = path;
    if (std::weak_ptr<core::Model<double>> weak_model = model;
        auto model_pointer = weak_model.lock()) {
      model_pointer->setTrajectory(&this->global_path, nullptr);
    }
  }
  /**
   * @fn std:s:tuple<double, int, double, double, double>
   * LQR_speed_steering_control(State<double> *state, std::vector<State<double>>
   * *st, double &pe, double &pth_e, std::vector<double> &sp)
   * @brief Main loop for LQR with speed control
   *
   * @param state Current state of the robot
   * @param st  All states of the trajectory
   * @param pe  deviation from trajectory
   * @param pth_e deviation from desire angle velocity
   * @param sp speed profile for all poses of the trajectory
   * @return
   */
  Eigen::VectorXd getSpeed(core::State<double> *state) override;

  inline void setCalculate(bool calculateLQRLocal) {
    calculateLQR = calculateLQRLocal;
    /// This is provide to reduce issue with using incorrect K matrix
    //            getFromFile(modelTypeToString(modelType), kCalculated);
  }

  /**
   * @brief set states for LQR
   * @param trajectory
   */
  void setStates(nav_msgs::msg::Path *trajectory_,
                 nav_msgs::msg::Path *trajectory_speed_) {
    this->trajectory = trajectory_;
    this->trajectory_speed = trajectory_speed_;

    if (std::weak_ptr<core::Model<double>> weak_model_ptr = model;
        auto ptr = weak_model_ptr.lock()) {
      ptr->setTrajectory(this->trajectory, this->trajectory_speed);
      ptr->setState(this->state);
    }
  };

  /**
   * @brief Set acceleration for trajectory if it needed
   * @param acceleration_ Trajectory Acceleration for all kind of states
   */
  void setAcceleration(nav_msgs::msg::Path *acceleration_) {
    this->trajectory_acc = acceleration_;

    if (std::weak_ptr<core::Model<double>> weak_model_ptr = model;
        auto ptr = weak_model_ptr.lock()) {
      ptr->setAcceleration(this->trajectory_acc);
    }
  }

  bool loadKMatrix(std::string &&file_name);

  /**
   * @fn void calc_nearest_index(State<double> *state,
   * std::vector<State<double>> *st)
   * @brief Find nearest pose on trajectory for current state
   * @param state  Current pose and state of the robot
   * @param pt Planner Trajectory class, which describes state of the system
   * (trajectory) from last build
   */
  virtual std::tuple<int, int> calculateNearestIdx(core::State<double> *state);

  virtual geometry_msgs::msg::Twist
  checkNewPosition(geometry_msgs::msg::Twist &cmdVel);

  ~LQR() = default;

  std::shared_ptr<core::Model<double>> model;
};
}; // namespace following_local_planner::plugins

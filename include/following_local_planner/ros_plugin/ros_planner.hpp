//
// Created by ristle on 1/28/22.
//

#pragma once

#include <bits/stdc++.h>
#include <following_local_planner/core/FollowPlanner.hpp>
#include <following_local_planner/functional/global_variables.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <nav2_core/controller.hpp>
#include <nav2_util/node_utils.hpp>
#include <nav_2d_msgs/msg/pose2_d_stamped.hpp>
#include <nav_2d_msgs/msg/twist2_d_stamped.hpp>
#include <nav_msgs/msg/path.hpp>
#include <pluginlib/class_loader.hpp>
#include <rclcpp/rclcpp.hpp>

namespace following_local_planner::ros_plugin {

    using rclcpp_lifecycle::LifecyclePublisher;

    class FollowingRosNode : public nav2_core::Controller {
    public:
        /**
         * @brief default constructor
         */
        FollowingRosNode();

        /// Честно хуй знает для чего это
        void configure(const rclcpp_lifecycle::LifecycleNode::WeakPtr &node,
                       std::string name, const std::shared_ptr<tf2_ros::Buffer> &tf,
                       const std::shared_ptr<nav2_costmap_2d::Costmap2DROS>
                       &costmap_ros);

        void activate() override;

        void deactivate() override;

        void cleanup() override;

        [[maybe_unused]] void setSpeedLimit([[maybe_unused]] const double &speed_limit,
                                            [[maybe_unused]] const bool &percentage) override { }

        /**
         * @brief nav2_core setPlan - Sets the global plan
         * @param path The global plan
         */
        void setPlan(const nav_msgs::msg::Path &path) override;

        /**
         * @brief nav2_core computeVelocityCommands - calculates the best command
         * given the current pose and velocity
         *
         * It is presumed that the global plan is already set.
         *
         * This is mostly a wrapper for the protected computeVelocityCommands
         * function which has additional debugging info.
         *
         * @param pose Current robot pose
         * @param velocity Current robot velocity
         * @return The best command for the robot to drive
         */
        geometry_msgs::msg::TwistStamped
        computeVelocityCommands(const geometry_msgs::msg::PoseStamped &pose,
                                const geometry_msgs::msg::Twist &velocity,
                                [[maybe_unused]] nav2_core::GoalChecker *goal_checker) override;

        /**
         * Bias path using some step is needed for better LQR following
         * @param path input path for bias
         * @param step This step for path biasing
         * @return
         */
        static nav_msgs::msg::Path pathBias(const nav_msgs::msg::Path &path,
                                            const double &step = 0.1);

    private:
        int frequency{30};
        double max_speed{1.};
        std::mutex main_loop_mutex;
        nav_msgs::msg::Path global_path;
        core::State<double> current_state;
        std::shared_ptr<tf2_ros::Buffer> tf_buffer;
        std::shared_ptr<nav2_costmap_2d::Costmap2DROS> cost_map_ros;
        rclcpp_lifecycle::LifecycleNode::WeakPtr ros_lifecycle_node;
        std::shared_ptr<following_local_planner::core::Follower> planner;

        std::shared_ptr<LifecyclePublisher<nav_msgs::msg::Path>> global_pub_;
        std::shared_ptr<LifecyclePublisher<nav_msgs::msg::Path>> local_pub_;

        /**
         * @brief Loop cycle to just compute linear velocity
         * @param pose current pose
         * @param velocity output velocity
         * @return
         */
        bool mainLoop(geometry_msgs::msg::Twist &velocity);

        /**
         * @brief update current state in cycle
         * @param pose current pose
         */
        void updateCurrentState(const geometry_msgs::msg::PoseStamped &pose);
    };
} // namespace following_local_planner::ros_plugin

#include <cstdio>
#include <rclcpp/rclcpp.hpp>

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  rclcpp::init(argc, argv);
  //    rclcpp::spin(std::make_shared<MinimalPublisher>());
  rclcpp::shutdown();

  printf("hello world following_local_planner package\n");
  return 0;
}

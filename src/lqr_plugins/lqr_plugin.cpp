//
// Created by ristle on 1/27/22.
//

#include <following_local_planner/lqr_plugins/lqr_planner.hpp>

#include <pluginlib/class_loader.hpp>

PLUGINLIB_EXPORT_CLASS(following_local_planner::plugins::LQR<double>,
                       following_local_planner::core::Follower)

namespace following_local_planner::plugins {
    using namespace std::placeholders;

    template<typename T>
    LQR<T>::LQR(double max_velocity, core::State<double> *stateLocal,
                [[maybe_unused]] T *costMap_)
            : following_local_planner::core::Follower() {
        // TODO: Remove hardcode of file path
        loadKMatrix("../../../param/K_Matrix.txt");
        pluginlib::ClassLoader<core::Model<double>> poly_loader(
                "lqr_system_plugins", "following_local_planner::core::Model<double>");
        try {
            model = poly_loader.createSharedInstance("DiffNoSpeedModel");
        } catch (pluginlib::PluginlibException &ex) {
            printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
        }
    };

    template<typename T>
    std::tuple<int, int>
    LQR<T>::calculateNearestIdx(core::State<double> *stateLocal) {
        double max_ = std::numeric_limits<double>::min();
        int index{0}, max_index{0};

        min_ = std::numeric_limits<double>::max();
        int trajectorySize = this->global_path.poses.size();

        double x, y, w;

        auto getPoint = [&](nav_msgs::msg::Path *pathLocal, int index) {
            x = pathLocal->poses[index].pose.position.x;
            y = pathLocal->poses[index].pose.position.y;

            double r, p;
            tf2::Quaternion quaternion_tf(pathLocal->poses[index].pose.orientation.x,
                                          pathLocal->poses[index].pose.orientation.y,
                                          pathLocal->poses[index].pose.orientation.z,
                                          pathLocal->poses[index].pose.orientation.w);

            tf2::Matrix3x3 rotation_matrix(quaternion_tf);
            rotation_matrix.getRPY(r, p, w);
        };

        for (int i = 0; i < trajectorySize; i++) {
            getPoint(&this->global_path, i);

            double temp_min = std::hypotf(x - stateLocal->x, y - stateLocal->y);
            if (min_ > temp_min) {
                min_ = temp_min;
                index = i;
            }
            if (max_ < temp_min) {
                max_ = temp_min;
                max_index = i;
            }
        }
        min_ = min_ == std::numeric_limits<double>::max() ? 0 : min_;
        min_ = std::sqrt(min_);

        getPoint(&this->global_path, index);

        double dxl = x - stateLocal->x;
        double dyl = y - stateLocal->y;

        if (std::atan(y - std::atan2(dyl, dxl)) < 0) {
            min_ *= -1;
        }
        return std::make_tuple(index, max_index);
    }

    template<typename T>
    Eigen::MatrixXd LQR<T>::solveDare(Eigen::MatrixXd *A, Eigen::MatrixXd *B,
                                      Eigen::MatrixXd *Q, Eigen::MatrixXd *R) {
        MatrixXd x = *Q;
        MatrixXd x_next = *Q;

        int max_iter = 100;
        double eps = 0.001;
        int count_of_iter{0};

        for (int i = 0; i < max_iter; i++) {
            Eigen::MatrixXd a_2_a = (A->transpose() * x) * (*A);
            Eigen::MatrixXd a_2_b = (A->transpose() * x) * (*B);
            Eigen::MatrixXd b_2_a = (B->transpose() * x) * (*A);
            Eigen::MatrixXd b_2_b = (B->transpose() * x) * (*B);

            x_next = a_2_a - (a_2_b * (*R + b_2_b.inverse())) * b_2_a + (*Q);

            double diff = fabs((x_next - x).maxCoeff());

            count_of_iter++;
            if (diff < eps) {
                return x_next;
            }
            x = x_next;
        }

        return x_next;
    }

    template<typename T>
    Eigen::MatrixXd LQR<T>::DLQR(Eigen::MatrixXd *A, Eigen::MatrixXd *B,
                                 Eigen::MatrixXd *Q, Eigen::MatrixXd *R) {
        MatrixXd X = solveDare(A, B, Q, R);

        Eigen::MatrixXd b_2_b = (B->transpose() * X) * (*B);
        Eigen::MatrixXd b_2_a = (B->transpose() * X) * (*A);
        Eigen::MatrixXd linear_solution = b_2_b.householderQr().solve(b_2_a);

        K = (*R) * linear_solution;

        if (K.hasNaN()) {
            throw std::runtime_error("K matrix");
        }
        return K;
    }

    template<typename T>
    Eigen::VectorXd LQR<T>::getSpeed(core::State<double> *stateLocal) {
        dt = 1. / this->frequency;
        int index, max_index{0};

        std::tie(index, max_index) = calculateNearestIdx(stateLocal);
        Eigen::VectorXd u, x_, c, ustar;

        if (std::weak_ptr<core::Model<double>> weak_model = model;
                auto model_pointer = weak_model.lock()) {
            std::tie(c, x_) = model_pointer->calculateError(stateLocal, index);
        }
        if (calculateLQR) {
            if (std::weak_ptr<core::Model<double>> weak_model = model;
                    auto model_pointer = weak_model.lock()) {
                std::tie(this->system_error_, this->desire_state) =
                        model_pointer->getStates();

                auto A = new Eigen::MatrixXd(3, 3);
                auto B = new Eigen::MatrixXd(3, 2);
                auto Q = new Eigen::MatrixXd(3, 3);
                auto R = new Eigen::MatrixXd(2, 2);

                std::tie(*A, *B) = model_pointer->getDynamicJacobianMatrices();
                std::tie(*Q, *R) =
                        model_pointer->getWeightMatrices(this->average_velocity, index);

                try {
                    K = DLQR(A, B, Q, R);
                } catch (...) {
                    K = Eigen::MatrixXd::Zero(Q->rows(), R->cols());
                }
                //     u = -uref - Kopt * (x - xref)
                try {
                    if (K.cols() == c.rows()) {
                        for (int i = 0; i < K.rows(); ++i) {
                            for (int j = 0; j < K.cols(); ++j) {
                                if (!isfinite(K(i, j))) {
                                    throw std::runtime_error("Fucked up");
                                }
                            }
                        }
                        ustar = -K * c;
                    } else {
                        throw std::runtime_error("Fucked up");
                    }
                } catch (...) {
                    ustar = Eigen::VectorXd::Zero(2);
               }

            } else {
                K = kCalculated;
                ustar = -K * c;
            }
        } else {
            K = kCalculated;
            ustar = -K * c;
        }
        ustar = model->additional(ustar, stateLocal);

        return ustar;
    }

    template<typename T>
    bool LQR<T>::loadKMatrix(std::string &&file_name) {
        std::ifstream fin(file_name);

        if (!fin.is_open()) {
            kCalculated = Eigen::MatrixXd(2, 5);
            for (int row = 0; row < 2; row++)
                for (int col = 0; col < 5; col++) {
                    double item = 0.0;
                    fin >> item;
                    kCalculated(row, col) = item;
                }
            fin.close();
            return true;
        } else {
            return false;
        }
    }

    template<typename T>
    geometry_msgs::msg::Twist
    LQR<T>::checkNewPosition(geometry_msgs::msg::Twist &cmdVel) {
        if (std::weak_ptr weakPtr = model; auto model_pointer = weakPtr.lock()) {
            //            std::shared_ptr<templateModules::Trajectory<costmap_2d::Costmap2D>>
            //            trajectory; /**< Init the trajectory generation */
            //            base_local_planner::Trajectory *baseTrajectoryLocal{};
            auto x = model_pointer->getCurrentState();
            Eigen::VectorXd u(2);

            u(0) = cmdVel.linear.z;
            u(1) = cmdVel.angular.z;

            x = model_pointer->integrateDynamics(x, u, this->dt);

            geometry_msgs::msg::PoseStamped newPose;

            newPose.pose.position.x = x(0);
            newPose.pose.position.y = x(1);
            newPose.pose.orientation.w = 1;

            newPose.header.frame_id = "map";
            newPose.header.stamp = rclcpp::Time();

            //            if (this->cost_map) {
            //                if (not CheckRotateTrajectory(&current_state, cmdVel,
            //                this->global_path)) {
            //                    cmdVel.linear.x = 0;
            //                    cmdVel.angular.z = 0;
            //                    return cmdVel;
            //                }
            /*
            unsigned int cell_x, cell_y;
            double x_global{x(0)}, y_global{x(1)};

            if (this->cost_map->worldToMap(x_global, y_global, cell_x, cell_y)) {
                double occCost = this->cost_map->getCost(cell_x, cell_y);
                if (occCost >= costmap_2d::INSCRIBED_INFLATED_OBSTACLE) {
                    return checkAngle();
                }
            } else {
                return checkAngle();
            }*/
            //            }
        }

        return cmdVel;
    }
}; // namespace following_local_planner::plugins

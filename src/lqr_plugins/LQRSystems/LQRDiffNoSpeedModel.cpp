//
// Created by ristle on 1/27/22.
//

#include <following_local_planner/lqr_plugins/LQRSystems/LQRDiffNoSpeedModel.hpp>

PLUGINLIB_EXPORT_CLASS(
        following_local_planner::plugins::DiffNoSpeedModel<double>,
        following_local_planner::core::Model<double>)

namespace following_local_planner::plugins {
    template<typename T>
    DiffNoSpeedModel<T>::DiffNoSpeedModel(nav_msgs::msg::Path *trajectory_,
                                          nav_msgs::msg::Path *trajectory_speed_,
                                          nav_msgs::msg::Path *trajectory_acc_)
            : core::Model<T>(trajectory_, trajectory_speed_, trajectory_acc_) {

    };

    template<typename T>
    std::tuple<Eigen::VectorXd, Eigen::VectorXd>
    DiffNoSpeedModel<T>::calculateErrorInternal(core::State<T> *state, int &index) {
        current_state = state;
        Eigen::VectorXd s_(3);
        Eigen::VectorXd x_(3);

        double x, y, w;

        std::tie(x, y, w) = this->getPoint(this->trajectory, index);

        s_ << state->x, state->y, state->yaw;
        x_ << x, y, w;

        Eigen::VectorXd output_error(3);
        output_error = (s_ - x_);

        output_error(2) = fmod(output_error(2), M_PI);

        this->system_error_ = output_error;
        this->desire_state = x_;

        return std::make_tuple(output_error, x_);
    }

    template<typename T>
    Eigen::VectorXd
    DiffNoSpeedModel<T>::getNextDesireOutputControls(unsigned int index) {
        double x, y, w;

        std::tie(x, y, w) = this->getPoint(this->trajectory, index);

        Eigen::VectorXd u = Eigen::VectorXd::Zero(2);
        u << std::hypot(x, y), w;

        return u;
    }

    template<typename T>
    std::tuple<Eigen::MatrixXd, Eigen::MatrixXd>
    DiffNoSpeedModel<T>::getWeightMatrices(double &max_speed, unsigned int index) {
        /**
         * In case of using simple mode
         * You need to turn next section on
         */
        this->index = getNextIndex(reinterpret_cast<int &>(index));
        /// Trying
        double time_to_stabilize =
                2 * fabs(reinterpret_cast<int &>(index) - this->index) /
                T(this->frequency);
        int founded_index{reinterpret_cast<int &>(index)};
        double x_, y_, w_;

        std::tie(x_, y_, w_) = this->getPoint(this->trajectory, 0);
        for (int i = 1; i < this->trajectory->poses.size(); ++i) {
            double x, y, w;

            std::tie(x, y, w) = this->getPoint(this->trajectory, i);

            if (fabs(w_) > 1e-33) {
                if (w < 1e-4) {
                    founded_index = i;
                } else {
                    break;
                }
            } else {
                if (w > 1e-4) {
                    founded_index = i;
                } else {
                    break;
                }
            }
        }
        double time_to_stabilize_angle =
                2 * fabs(founded_index - this->index) / T(this->frequency);

        Eigen::MatrixXd R = functional::setIdentityMatrix(std::make_tuple(
                following_local_planner::functional::prepareR<double>(max_speed, time_to_stabilize),
                following_local_planner::functional::prepareR<double>(max_speed, time_to_stabilize_angle)));

        Eigen::MatrixXd Q = functional::setIdentityMatrix(std::make_tuple(
                functional::prepareQ<double>((MAX_GLOBAL_SPEED - max_speed),
                                             time_to_stabilize),
                functional::prepareQ<double>((MAX_GLOBAL_SPEED - max_speed),
                                             time_to_stabilize),
                functional::prepareQ<double>((MAX_GLOBAL_SPEED - max_speed),
                                             time_to_stabilize_angle)));
        Q.setIdentity(3, 3) * 1;
        R.setIdentity(2, 2) * 1;

        return std::make_tuple(Q, R);
    }

    template<typename T>
    Eigen::VectorXd DiffNoSpeedModel<T>::dynamics(const Eigen::VectorXd &x,
                                                  const Eigen::VectorXd &u,
                                                  double dt) {
        Eigen::VectorXd dynamic_state(3);

        dynamic_state(0) = u(0) * std::cos(x(2)) * dt;
        dynamic_state(1) = u(0) * std::sin(x(2)) * dt;
        dynamic_state(2) = u(1) * dt;

        return dynamic_state;
    }

    template<typename T>
    int DiffNoSpeedModel<T>::getNextIndex(int &indexLocal) {
        return reinterpret_cast<int &>(indexLocal) +
               int(std::ceil(this->frequency * 0.1));
    }

    template<typename T>
    std::tuple<Eigen::VectorXd, Eigen::VectorXd>
    DiffNoSpeedModel<T>::calculateError(core::State<T> *state, int &index) try {
        current_state = state;

        this->index = getNextIndex(index);
        return calculateErrorInternal(state, this->index);
    } catch (std::bad_alloc &error) {
        return calculateErrorInternal(state,
                                      std::move(this->trajectory->poses.size() - 1));
    }

    template<typename T>
    Eigen::VectorXd DiffNoSpeedModel<T>::additional(Eigen::VectorXd &uStar,
                                                    core::State<T> *stateLocal) {
//        Eigen::VectorXd output(2);
//        Eigen::MatrixXd ALocal, BLocal;
//        Eigen::VectorXd currentStateLocal(3);
//
//        currentStateLocal << stateLocal->x, stateLocal->y, stateLocal->yaw;
//        std::tie(ALocal, BLocal) = this->getDynamicJacobianMatrices();
//
//        auto derivative = ALocal * currentStateLocal + BLocal * uStar;
//        output << std::hypot(derivative(0), derivative(1)), derivative(2);

        return uStar;
    }

    template
    class DiffNoSpeedModel<double>;

}; // namespace following_local_planner::plugins

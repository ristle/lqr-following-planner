//
// Created by ristle on 3/19/22.
//

//
// Created by ristle on 1/27/22.
//

#include <following_local_planner/lqr_plugins/lqr_acc_planner.hpp>
#include <openacc.h>

PLUGINLIB_EXPORT_CLASS(following_local_planner::plugins::LQR_ACC<double>,
                       following_local_planner::core::Follower)

namespace following_local_planner::plugins {
    using namespace std::placeholders;

    template<typename T>
    LQR_ACC<T>::LQR_ACC(double max_velocity, core::State<double> *stateLocal,
                        [[maybe_unused]] T *costMap_)
            : following_local_planner::core::Follower() {
        try {
            pluginlib::ClassLoader<core::Model<double>> poly_loader(
                    "lqr_system_plugins", "following_local_planner::core::Model<double>");
            model = poly_loader.createSharedInstance("DiffNoSpeedModel");
        } catch (pluginlib::PluginlibException &ex) {
            printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
        }
    };

    template<typename T>
    void LQR_ACC<T>::reduction_matrix(
            boost::numeric::ublas::matrix<double> *matrix_first,
            boost::numeric::ublas::matrix<double> *matrix_second,
            boost::numeric::ublas::matrix<double> *output_matrix) {
        output_matrix->resize(matrix_first->size1(), matrix_second->size2());
#pragma acc parallel copyin(matrix_first, matrix_second) create(output_matrix)
        {
#pragma acc loop gang worker vector collapse(2)
            for (size_t i = 0; i < matrix_first->size1(); ++i) {
                for (size_t j = 0; j < matrix_second->size2(); ++j) {
                    (*output_matrix)(i, j) = (*matrix_first)(i, j) + (*matrix_second)(i, j);
                }
            }
        }
#pragma acc exit data copyout(output_matrix)
    }

    template<typename T>
    void LQR_ACC<T>::minus_matrix(
            boost::numeric::ublas::matrix<double> *matrix_first,
            boost::numeric::ublas::matrix<double> *matrix_second,
            boost::numeric::ublas::matrix<double> *output_matrix) {
        output_matrix->resize(matrix_first->size1(), matrix_second->size2());
#pragma acc parallel copyin(matrix_first, matrix_second) create(output_matrix)
        {
#pragma acc loop gang worker collapse(2)
            for (size_t i = 0; i < matrix_first->size1(); ++i) {
                for (size_t j = 0; j < matrix_second->size2(); ++j) {
                    (*output_matrix)(i, j) = (*matrix_first)(i, j) - (*matrix_second)(i, j);
                }
            }
        }
#pragma acc exit data copyout(output_matrix)
    }

    template<typename T>
    void LQR_ACC<T>::multiply_matrix(
            boost::numeric::ublas::matrix<double> *matrix_first,
            boost::numeric::ublas::matrix<double> *matrix_second,
            boost::numeric::ublas::matrix<double> *output_matrix) {
        output_matrix->resize(matrix_first->size1(), matrix_second->size2());
#pragma acc enter data copyin(matrix_first, matrix_second) create(output_matrix)
#pragma acc parallel
        {

#pragma acc loop gang worker collapse(2)
            for (size_t i = 0; i < matrix_first->size1(); ++i) {
                for (size_t j = 0; j < matrix_second->size2(); ++j) {
                    (*output_matrix)(i, j) = 0;
                }
            }

#pragma acc loop gang vector worker collapse(3)
            for (size_t i = 0; i < matrix_first->size1(); ++i) {
                for (size_t j = 0; j < matrix_second->size2(); ++j) {
                    (*output_matrix)(i, j) = 0;
                    for (size_t k = 0; k < matrix_second->size1(); ++k) {
                        (*output_matrix)(i, j) += (*matrix_first)(i, k) * (*matrix_second)(k, j);
                    }
                }
            }
        }
#pragma acc exit data copyout(output_matrix)
    }

    template<typename T>
    void LQR_ACC<T>::multiply_matrix(
            boost::numeric::ublas::matrix<double> *matrix_first,
            boost::numeric::ublas::matrix<double> *matrix_second,
            boost::numeric::ublas::matrix<double> *matrix_third,
            boost::numeric::ublas::matrix<double> *output_matrix
    ) {

        boost::numeric::ublas::matrix<double> result_first_multiply;

        multiply_matrix(matrix_first, matrix_second, &result_first_multiply);
        multiply_matrix(&result_first_multiply, matrix_third, output_matrix);
    }


    template<typename T>
    std::tuple<int, int>
    LQR_ACC<T>::calculateNearestIdx(core::State<double> *stateLocal) {
        double max_ = std::numeric_limits<double>::min();
        int index{0}, max_index{0};

        min_ = std::numeric_limits<double>::max();
        int trajectorySize = this->global_path.poses.size();

        double x, y, w;

        auto getPoint = [&](nav_msgs::msg::Path *pathLocal, int index) {
            x = pathLocal->poses[index].pose.position.x;
            y = pathLocal->poses[index].pose.position.y;

            double r, p;
            tf2::Quaternion quaternion_tf(pathLocal->poses[index].pose.orientation.x,
                                          pathLocal->poses[index].pose.orientation.y,
                                          pathLocal->poses[index].pose.orientation.z,
                                          pathLocal->poses[index].pose.orientation.w);

            tf2::Matrix3x3 rotation_matrix(quaternion_tf);
            rotation_matrix.getRPY(r, p, w);
        };

        for (int i = 0; i < trajectorySize; i++) {
            getPoint(&this->global_path, i);

            double temp_min = std::hypotf(x - stateLocal->x, y - stateLocal->y);
            if (min_ > temp_min) {
                min_ = temp_min;
                index = i;
            }
            if (max_ < temp_min) {
                max_ = temp_min;
                max_index = i;
            }
        }
        min_ = min_ == std::numeric_limits<double>::max() ? 0 : min_;
        min_ = std::sqrt(min_);

        getPoint(&this->global_path, index);

        double dxl = x - stateLocal->x;
        double dyl = y - stateLocal->y;

        if (std::atan(y - std::atan2(dyl, dxl)) < 0) {
            min_ *= -1;
        }

        return std::make_tuple(index, max_index);
    }

    template<typename T>
    boost::numeric::ublas::matrix<double> LQR_ACC<T>::solveDare(
            boost::numeric::ublas::matrix<double> *A, boost::numeric::ublas::matrix<double> *B,
            boost::numeric::ublas::matrix<double> *Q, boost::numeric::ublas::matrix<double> *R) {
        auto x_next = new boost::numeric::ublas::matrix<double>();
        auto x = new boost::numeric::ublas::matrix<double>();
        *x_next = *Q;
        *x = *Q;

        double eps = 0.001;
        int max_iter = 100;
        int count_of_iter{0};

#pragma acc data copyin(x, max_iter, eps, A, B, Q, R)  copyout(x_next)
        {
            bool exit_the_loop = false;
            for (int i = 0; i < max_iter; i++) {
                if (exit_the_loop) {
                    continue;
                }
                bool success;

                auto a_2_a_part = boost::numeric::ublas::matrix<double>();
                auto a_2_b_part = boost::numeric::ublas::matrix<double>();
                auto b_2_a_part = boost::numeric::ublas::matrix<double>();
                auto b_2_b_part = boost::numeric::ublas::matrix<double>();
                auto second_part = boost::numeric::ublas::matrix<double>();
                auto middle_part = boost::numeric::ublas::matrix<double>();
                auto inverse_part = boost::numeric::ublas::matrix<double>();
                auto reduction_result = boost::numeric::ublas::matrix<double>();
                auto reduction_second_part = boost::numeric::ublas::matrix<double>();
                boost::numeric::ublas::matrix<double> A_T = boost::numeric::ublas::trans(*A);
                boost::numeric::ublas::matrix<double> B_T = boost::numeric::ublas::trans(*B);

                this->multiply_matrix(&A_T, x, A, &a_2_a_part);
                this->multiply_matrix(&A_T, x, B, &a_2_b_part);
                this->multiply_matrix(&B_T, x, A, &b_2_a_part);
                this->multiply_matrix(&B_T, x, B, &b_2_b_part);
                following_local_planner::functional::get_inverse<>(&b_2_b_part, &inverse_part);
                this->reduction_matrix(R, &inverse_part, &reduction_result);

                this->multiply_matrix(&a_2_b_part, &reduction_result, &b_2_a_part, &second_part);
                this->minus_matrix(&a_2_a_part, &second_part, &middle_part);
                this->reduction_matrix(&middle_part, Q, x_next);

                /// Max value of the value
                double diff = std::numeric_limits<double>::min();
                auto x_diff = boost::numeric::ublas::matrix<double>();
                this->minus_matrix(x_next, x, &x_diff);

#pragma acc parallel collapse(2)
                for (int k = 0; k < (int) x_diff.size1(); ++k) {
                    for (int j = 0; j < (int) x_diff.size2(); ++j) {
                        if (x_diff(k, j) > diff) {
                            diff = x_diff(k, j);
                        }
                    }
                }
                if (fabs(diff) < eps) {
#pragma acc atomic
                    exit_the_loop = true;
                }
                count_of_iter++;
                *x = *x_next;
            }
        }
        return *x_next;
    }

    template<typename T>
    boost::numeric::ublas::matrix<double> LQR_ACC<T>::get_boost_matrix_from_eigen(Eigen::MatrixXd &matrix_eigen) {
        boost::numeric::ublas::matrix<double> output_matrix(matrix_eigen.rows(), matrix_eigen.cols());
        for (long i = 0; i < matrix_eigen.rows(); i++) {
            for (long j = 0; j < matrix_eigen.cols(); j++) {
                output_matrix(i, j) = matrix_eigen(i, j);
            }
        }
        return output_matrix;
    }

    template<typename T>
    boost::numeric::ublas::matrix<double> LQR_ACC<T>::get_boost_matrix_from_eigen(Eigen::MatrixXd *matrix_eigen) {
        boost::numeric::ublas::matrix<double> output_matrix(matrix_eigen->rows(), matrix_eigen->cols());
        for (long i = 0; i < matrix_eigen->rows(); i++) {
            for (long j = 0; j < matrix_eigen->cols(); j++) {
                output_matrix(i, j) = (*matrix_eigen)(i, j);
            }
        }
        return output_matrix;
    }

    template<typename T>
    Eigen::MatrixXd LQR_ACC<T>::get_eigen_matrix_from_boost(boost::numeric::ublas::matrix<double> &matrix_boost) {
        Eigen::MatrixXd output_matrix(matrix_boost.size1(), matrix_boost.size2());
        for (size_t i = 0; i < matrix_boost.size1(); i++) {
            for (size_t j = 0; j < matrix_boost.size2(); j++) {
                output_matrix(i, j) = matrix_boost(i, j);
            }
        }
        return output_matrix;
    }

    template<typename T>
    Eigen::MatrixXd LQR_ACC<T>::get_eigen_matrix_from_boost(boost::numeric::ublas::matrix<double> *matrix_boost) {
        Eigen::MatrixXd output_matrix(matrix_boost->size1(), matrix_boost->size2());
        for (size_t i = 0; i < matrix_boost->size1(); i++) {
            for (size_t j = 0; j < matrix_boost->size2(); j++) {
                output_matrix(i, j) = (*matrix_boost)(i, j);
            }
        }
        return output_matrix;
    }

    template<typename T>
    Eigen::MatrixXd
    LQR_ACC<T>::DLQR(boost::numeric::ublas::matrix<double> *A, boost::numeric::ublas::matrix<double> *B,
                     boost::numeric::ublas::matrix<double> *Q, boost::numeric::ublas::matrix<double> *R) {
        // TODO: COPY IT to device
        auto X = solveDare(A, B, Q, R);

        bool failure;

        boost::numeric::ublas::matrix<double> b_2_b_part;
        boost::numeric::ublas::matrix<double> b_2_a_part;
        boost::numeric::ublas::matrix<double> k_from_dlqr;
        boost::numeric::ublas::matrix<double> linear_solution;
        boost::numeric::ublas::matrix<double> b_2_b_inverse_part;
        boost::numeric::ublas::matrix<double> inverse_linear_part;
        boost::numeric::ublas::matrix<double> B_T = boost::numeric::ublas::trans(*B);

        this->multiply_matrix(&B_T, &X, A, &b_2_a_part);
        this->multiply_matrix(&B_T, &X, B, &b_2_b_part);

        // Solve xA = y;
        following_local_planner::functional::get_inverse<>(&b_2_b_part, &b_2_b_inverse_part);
        this->multiply_matrix(&b_2_b_inverse_part, &b_2_a_part, &linear_solution);

        if (failure) {
            throw std::runtime_error("Fucked_up");
        }

        /// K = R * (trans(A) * A)^-1 * trans(A) * y;
        this->multiply_matrix(R,
                              &linear_solution,
                              &k_from_dlqr);

        return get_eigen_matrix_from_boost(k_from_dlqr);
    }

    template<typename T>
    Eigen::VectorXd LQR_ACC<T>::getSpeed(core::State<double> *stateLocal) {
        dt = 1. / this->frequency;
        int index, max_index{0};

        std::tie(index, max_index) = calculateNearestIdx(stateLocal);
        Eigen::VectorXd u, x_, c, ustar;

        if (std::weak_ptr<core::Model<double>> weak_model = model;
                auto model_pointer = weak_model.lock()) {
            std::tie(c, x_) = model_pointer->calculateError(stateLocal, index);
        }
        if (calculateLQR) {
            if (std::weak_ptr<core::Model<double>> weak_model = model;
                    auto model_pointer = weak_model.lock()) {
                std::tie(this->system_error_, this->desire_state) =
                        model_pointer->getStates();

                auto A = new Eigen::MatrixXd();
                auto B = new Eigen::MatrixXd();
                auto Q = new Eigen::MatrixXd();
                auto R = new Eigen::MatrixXd();

                auto A_boost = new boost::numeric::ublas::matrix<double>();
                auto B_boost = new boost::numeric::ublas::matrix<double>();
                auto Q_boost = new boost::numeric::ublas::matrix<double>();
                auto R_boost = new boost::numeric::ublas::matrix<double>();

#pragma acc data enter copyin(A, B, Q, R) copyout(A_boost, B_boost, Q_boost, R_boost)
                {
#pragma acc  async(1)
                    std::tie(*A, *B) = model_pointer->getDynamicJacobianMatrices();
#pragma acc  async(2)
                    std::tie(*Q, *R) =
                            model_pointer->getWeightMatrices(this->average_velocity, index);
#pragma acc  wait(1) async(3)
                    *A_boost = get_boost_matrix_from_eigen(A);
#pragma acc  wait(1) async(4)
                    *B_boost = get_boost_matrix_from_eigen(B);
#pragma acc  wait(2) async(5)
                    *Q_boost = get_boost_matrix_from_eigen(Q);
#pragma acc  wait(2) async(6)
                    *R_boost = get_boost_matrix_from_eigen(R);
                }
#pragma acc wait

                try {
                    K = DLQR(A_boost, B_boost, Q_boost, R_boost);
                } catch (std::runtime_error &e) {
                    K = Eigen::MatrixXd::Zero(Q_boost->size1(), R_boost->size2());
                }

                if (K.cols() == c.rows()) {
                    ustar = -K * c;
                } else {
                    ustar = Eigen::VectorXd::Zero(2);
                }

                delete A;
                delete B;
                delete Q;
                delete R;

                delete A_boost;
                delete B_boost;
                delete Q_boost;
                delete R_boost;

            } else {
                K = kCalculated;
                ustar = -K * c;
            }
        } else {
            K = kCalculated;
            ustar = -K * c;
        }
        ustar = model->additional(ustar, stateLocal);

        return ustar;
    }

    template<typename T>
    bool LQR_ACC<T>::loadKMatrix(std::string &&file_name) {
        std::ifstream fin(file_name);

        if (!fin.is_open()) {
            kCalculated = Eigen::MatrixXd(2, 5);
            for (int row = 0; row < 2; row++)
                for (int col = 0; col < 5; col++) {
                    double item = 0.0;
                    fin >> item;
                    kCalculated(row, col) = item;
                }
            fin.close();
            return true;
        } else {
            return false;
        }
    }

    template<typename T>
    geometry_msgs::msg::Twist
    LQR_ACC<T>::checkNewPosition(geometry_msgs::msg::Twist &cmdVel) {
        if (std::weak_ptr weakPtr = model; auto model_pointer = weakPtr.lock()) {
            //            std::shared_ptr<templateModules::Trajectory<costmap_2d::Costmap2D>>
            //            trajectory; /**< Init the trajectory generation */
            //            base_local_planner::Trajectory *baseTrajectoryLocal{};
            auto x = model_pointer->getCurrentState();
            Eigen::VectorXd u(2);

            u(0) = cmdVel.linear.z;
            u(1) = cmdVel.angular.z;

            x = model_pointer->integrateDynamics(x, u, this->dt);

            geometry_msgs::msg::PoseStamped newPose;

            newPose.pose.position.x = x(0);
            newPose.pose.position.y = x(1);
            newPose.pose.orientation.w = 1;

            newPose.header.frame_id = "map";
            newPose.header.stamp = rclcpp::Time();

            //            if (this->cost_map) {
            //                if (not CheckRotateTrajectory(&current_state, cmdVel,
            //                this->global_path)) {
            //                    cmdVel.linear.x = 0;
            //                    cmdVel.angular.z = 0;
            //                    return cmdVel;
            //                }
            /*
            unsigned int cell_x, cell_y;
            double x_global{x(0)}, y_global{x(1)};

            if (this->cost_map->worldToMap(x_global, y_global, cell_x, cell_y)) {
                double occCost = this->cost_map->getCost(cell_x, cell_y);
                if (occCost >= costmap_2d::INSCRIBED_INFLATED_OBSTACLE) {
                    return checkAngle();
                }
            } else {
                return checkAngle();
            }*/
            //            }
        }

        return cmdVel;
    }

}; // namespace following_local_planner::plugins

//
// Created by ristle on 3/12/22.
//


#include <following_local_planner/lqr_plugins/ilqr_plugin.hpp>

namespace following_local_planner::plugins {
    // TODO: Parallelize it using openACC
    /// createTrajectory, localCost, quadratizeObstacleCost, quadratizeCost, quadratizeFinalCost,
    /// regularize

    static const double DEFAULT_STEP_SIZE = 0.0009765625;
    template<int Dim>
    using Vector = Eigen::Matrix<double, Dim, 1>;

    template<int Dim>
    using SymmetricMatrix = Eigen::Matrix<double, Dim, Dim>;

    template<int rDim, int cDim>
    using Matrix = Eigen::Matrix<double, rDim, cDim>;

    iLQR::iLQR(int frequency_,
               double max_velocity_,
               double average_velocity_,
               double *cost_map_,
               const std::string &modelTypeLocal) : following_local_planner::core::Trajectory(frequency_,
                                                                                              max_velocity_,
                                                                                              average_velocity_,
                                                                                              cost_map_) {
        pluginlib::ClassLoader<core::Model<double>> poly_loader(
                "following_local_planner", "following_local_planner::core::Model<double>");
        try {
            model = poly_loader.createSharedInstance(modelTypeLocal);
        } catch (pluginlib::PluginlibException &ex) {
            printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
        }
    }


    iLQR::iLQR(int frequency_,
               double max_velocity_,
               double average_velocity_) : core::Trajectory(frequency_, max_velocity_,
                                                            average_velocity_) {
        try {
            pluginlib::ClassLoader<core::Model<double>> poly_loader(
                    "following_local_planner", "following_local_planner::core::Model<double>");
            model = poly_loader.createSharedInstance("DiffNoSpeedModel");
        } catch (pluginlib::PluginlibException &ex) {
            printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
        }
    }


    iLQR::iLQR(int *frequency_,
               double *max_velocity_,
               double *average_velocity_) : core::Trajectory(frequency_, max_velocity_,
                                                             average_velocity_) {
        pluginlib::ClassLoader<core::Model<double>> poly_loader(
                "following_local_planner", "following_local_planner::core::Model<double>");
        try {
            model = poly_loader.createSharedInstance("DiffNoSpeedModel");
        } catch (pluginlib::PluginlibException &ex) {
            printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
        }
    }


    void iLQR::createTrajectory(nav_msgs::msg::Path &path) {
        Eigen::VectorXd u, x_, c, ustar;
        if (std::weak_ptr<core::Model<double>> weak_model = model; auto model_pointer = weak_model.lock()) {
            model_pointer->setTrajectory(&path, &path);
        }

        this->xGoal << path.poses[1].pose.position.x, path.poses[1].pose.position.y, tf2::getYaw(
                path.poses[1].pose.orientation);
        this->xStart << path.poses[0].pose.position.x, path.poses[0].pose.position.y, tf2::getYaw(
                path.poses[0].pose.orientation);

        SymmetricMatrix<X_DIM> A;
        Matrix<X_DIM, U_DIM> B;
        if (std::weak_ptr<core::Model<double>> weak_model = model; auto model_pointer = weak_model.lock()) {
            Eigen::MatrixXd ALocal, BLocal, QLocal, RLocal;

            std::tie(ALocal, BLocal) = model_pointer->getDynamicJacobianMatrices();
//            std::tie(QLocal, RLocal) = model_pointer->getWeightMatrices(this->average_velocity, 40);

            A = ALocal;
            B = BLocal;
            R = 0.5 * Eigen::MatrixXd::Identity(2, 2);
            Q = Eigen::MatrixXd::Identity(3, 3);
        }

        double length = 0;
        for (int i = 0; i < int(path.poses.size() - 1); ++i) {
            length += std::hypot(path.poses[i].pose.position.x - path.poses[i + 1].pose.position.x,
                                 path.poses[i].pose.position.y - path.poses[i + 1].pose.position.y);
        }
        int num_iter = length / global_vars::getMaxSpeed() * global_vars::getFrequency();
        int maxIter = num_iter * 1.5;

        L.resize(num_iter, Matrix<U_DIM, X_DIM>::Zero());
        l.resize(num_iter, uNominal);

        std::vector<Vector<X_DIM> > xHat(num_iter + 1, Vector<X_DIM>::Zero());
        std::vector<Vector<X_DIM> > xHatNew(num_iter + 1, Vector<X_DIM>::Zero());
        std::vector<Vector<U_DIM> > uHat(num_iter);
        std::vector<Vector<U_DIM> > uHatNew(num_iter);

        double oldCost = -log(0.0);

        for (iter = 0; iter < maxIter; ++iter) {
            double newCost;
            double alpha = 1.0;

            // Forward pass to get nominal trajectory
            do {
                newCost = 0;

                // initialize trajectory
                xHatNew[0] = this->xStart;
                for (int t = 0; t < num_iter; ++t) {
                    // Compute control
                    uHatNew[t] = (1.0 - alpha) * uHat[t] + L[t] * (xHatNew[t] - (1.0 - alpha) * xHat[t]) + alpha * l[t];
                    // Forward one-step

                    if (std::weak_ptr<core::Model<double>> weak_model = model; auto model_pointer = weak_model.lock()) {
                        xHatNew[t + 1] = model_pointer->integrateDynamics(xHatNew[t], uHatNew[t],
                                                                          1. / global_vars::getFrequency());
                    }

                    // compute cost
                    newCost += this->localCost(xHatNew[t], uHatNew[t], t);
                }
                // Compute final state cost
                newCost += this->cell(xHatNew[num_iter]);
                //exit(0);

//                 Decrease alpha, if the new cost is not less than old cost
                alpha *= 0.5;
            } while (!(newCost < oldCost || fabs((oldCost - newCost) / newCost) < 1e-4));

            xHat = xHatNew;
            uHat = uHatNew;

            if (fabs((oldCost - newCost) / newCost) < 1e-4) {
                // No significant improvement in cost
                return;
            }

            oldCost = newCost;

            // backward pass to compute updates to control
            SymmetricMatrix<X_DIM> S;
            Vector<X_DIM> s;  // v, in notes

            // compute final cost  S_N = Q_f
            this->quadratizeFinalCost(xHat[num_iter], S, s, iter);

            for (int t = num_iter - 1; t != -1; --t) {

                const Vector<X_DIM> c = xHat[t + 1] - A * xHat[t] - B * uHat[t];  // error in linearization

                Matrix<U_DIM, X_DIM> P;
                SymmetricMatrix<X_DIM> Q;
                SymmetricMatrix<U_DIM> R;
                Vector<X_DIM> q;
                Vector<U_DIM> r;

                // Quadratize the cost
                this->quadratizeCost(xHat[t], uHat[t], t, P, Q, R, q, r, iter);

                const Matrix<U_DIM, X_DIM> C = B.transpose() * S * A + P;
                const SymmetricMatrix<X_DIM> D = A.transpose() * (S * A) + Q;
                const SymmetricMatrix<U_DIM> E = B.transpose() * (S * B) + R;
                const Vector<X_DIM> d = A.transpose() * (s + S * c) + q;
                const Vector<U_DIM> e = B.transpose() * (s + S * c) + r;

                L[t] = -(E.colPivHouseholderQr().solve(C));
                l[t] = -(E.colPivHouseholderQr().solve(e));

                S = D + C.transpose() * L[t];
                s = d + C.transpose() * l[t];
            }
        }
        std::cout << l.size() << std::endl;
        for (int i = 0; i < (int) L.size(); ++i) {
            auto newPose = this->xStart;
            if (std::weak_ptr<core::Model<double>> weak_model = model; auto model_pointer = weak_model.lock()) {
                newPose = model_pointer->integrateDynamics(newPose, L[i] * newPose + l[i],
                                                           1. / global_vars::getFrequency());
            }
            this->trajectory.poses.push_back(
                    geometry_msgs::msg::PoseStamped().set__pose(
                            geometry_msgs::msg::Pose().set__position(
                                    geometry_msgs::msg::Point()
                                            .set__x(newPose[0])
                                            .set__y(newPose[1])
                                            .set__z(newPose[2]))
                    )
            );

            double x = l[i][0] * std::cos(newPose[2]);
            double y = l[i][0] * std::sin(newPose[2]);
            double w = l[i][1];
            this->trajectory_speed.poses.push_back(
                    geometry_msgs::msg::PoseStamped().set__pose(
                            geometry_msgs::msg::Pose().set__position(
                                    geometry_msgs::msg::Point()
                                            .set__x(x)
                                            .set__y(y)
                                            .set__z(w))
                    )
            );
        }
        for (int i = 0; i < (int) this->trajectory.poses.size(); ++i) {
            double x, y, w;
            x = this->trajectory.poses[i].pose.position.x;
            y = this->trajectory.poses[i].pose.position.y;
            w = tf2::getYaw(this->trajectory.poses[i].pose.orientation);
        }
    }


    double iLQR::localCost(const Vector<X_DIM> &x, const Vector<U_DIM> &u, const int &t) {
        double cost = 0;
        if (!t) {
            cost += ((x - this->xStart).transpose() * this->Q * (x - this->xStart));
        }
        cost += ((u - this->uNominal).transpose() * this->R * (u - this->uNominal));
        return cost;
    }


    void iLQR::quadratizeObstacleCost(const Vector<X_DIM> &x, SymmetricMatrix<X_DIM> &Q, Vector<X_DIM> &q) {
        SymmetricMatrix<DIM> QObs = SymmetricMatrix<DIM>::Zero();
        Vector<DIM> qObs = Vector<DIM>::Zero();

        for (int i = 0; i < 0; ++i) {
            Vector<DIM> d = x.head(DIM)/* - this->obstacles[i].pos */;
            double distr = sqrt(d.dot(d.transpose()));
            d /= distr;
            double dist = distr - 0.4 - 0.6;

            Vector<DIM> d_ortho;
            d_ortho[0] = d[1];
            d_ortho[1] = -d[0];

            double a0 = exp(-1 * dist);
            double a1 = -1 * a0;
            double a2 = -1 * a1;

            QObs += a2 * (d * d.transpose());
            qObs += a1 * d;
        }
        for (int i = 0; i < DIM; ++i) {
            double dist = (4 - x[i]) - 0.4;

            Vector<DIM> d = Vector<DIM>::Zero();
            d[i] = -1.0;

            double a0 = exp(-1 * dist);
            double a1 = -1 * a0;
            double a2 = -1 * a1;

            QObs += a2 * (d * d.transpose());
            qObs += a1 * d;
        }

        this->regularize(QObs);
        //Q.insert(0, QObs + Q.subSymmetricMatrix<DIM>(0));
        Q.block<DIM, DIM>(0, 0) = QObs + Q.block<DIM, DIM>(0, 0);
        //q.insert(0,0, qObs - QObs*x.subMatrix<DIM>(0,0) + q.subMatrix<DIM>(0,0));
        q.head(DIM) = qObs - QObs * x.head(DIM) + q.head(DIM);
    }


    void iLQR::quadratizeCost(const Vector<X_DIM> &x, const Vector<U_DIM> &u, const int &t,
                              Matrix<U_DIM, X_DIM> &Pt, SymmetricMatrix<X_DIM> &Qt,
                              SymmetricMatrix<U_DIM> &Rt, Vector<X_DIM> &qt, Vector<U_DIM> &rt,
                              const int &iter) {
        /*Qt = hessian1(x, u, t, c);
          Pt = ~hessian12(x, u, t, c);
          Rt = hessian2(x, u, t, c);
          qt = jacobian1(x, u, t, c) - Qt*x - ~Pt*u;
          rt = jacobian2(x, u, t, c) - Pt*x - Rt*u;*/
        if (t == 0) {
            Qt = this->Q;
            qt = -(this->Q * this->xStart);
        } else {
            Qt = SymmetricMatrix<X_DIM>::Zero();
            qt = Vector<X_DIM>::Zero();

            if (iter < 2) {
                Qt(2, 2) = 1;
                qt[2] = -1 * (M_PI / 2);
            }
        }
        Rt = this->R;
        rt = -(this->R * this->uNominal);
        Pt = Matrix<U_DIM, X_DIM>::Zero();

        this->quadratizeObstacleCost(x, Qt, qt);
    }

// Final cost function c_\num_iter(x_\num_iter)

    double iLQR::cell(const Vector<X_DIM> &x) {
        double cost = 0;
        cost += ((x - this->xGoal).transpose() * this->Q * (x - this->xGoal));
        return cost;
    }


    void
    iLQR::quadratizeFinalCost(const Vector<X_DIM> &x, SymmetricMatrix<X_DIM> &Qell,
                              Vector<X_DIM> &qell,
                              const int &iter) {
        /*Qnum_iter = hessian(x, cnum_iter);
          qnum_iter = jacobian(x, cnum_iter) - Qnum_iter*x;*/
        Qell = this->Q;
        qell = -(this->Q * this->xGoal);
    }


    void iLQR::regularize(SymmetricMatrix<DIM> &Q) {
        SymmetricMatrix<DIM> D;
        Matrix<DIM, DIM> V;
        //jacobi(Q, V, D);
        Eigen::EigenSolver<SymmetricMatrix<DIM> > es(Q);
        D = es.pseudoEigenvalueMatrix();
        V = es.pseudoEigenvectors();
        for (int i = 0; i < DIM; ++i) {
            if (D(i, i) < 0) {
                D(i, i) = 0;
            }
        }
        Q = V * (D * V.transpose());
    }


    [[maybe_unused]] Vector<X_DIM> iLQR::getNewPose(const Vector<X_DIM> &x, const Vector<U_DIM> &u) {
        if (std::weak_ptr<core::Model<double>> weak_model = model; auto model_pointer = weak_model.lock()) {
            Vector<X_DIM> k1 = model_pointer->dynamics(x, u, 1. / global_vars::getFrequency());
            Vector<X_DIM> k2 = model_pointer->dynamics(x + 0.5 * this->dt * k1, u, 1. / global_vars::getFrequency());
            Vector<X_DIM> k3 = model_pointer->dynamics(x + 0.5 * this->dt * k2, u, 1. / global_vars::getFrequency());
            Vector<X_DIM> k4 = model_pointer->dynamics(x + this->dt * k3, u, 1. / global_vars::getFrequency());
            return x + (this->dt / 6.0) * (k1 + 2.0 * k2 + 2.0 * k3 + k4);
        }
        return x;
    }
}
//
// Created by ristle on 1/28/22.
//
#include <following_local_planner/ros_plugin/ros_planner.hpp>
#include <pluginlib/class_list_macros.hpp>
#include <pluginlib/class_loader.hpp>

PLUGINLIB_EXPORT_CLASS(following_local_planner::ros_plugin::FollowingRosNode,
                       nav2_core::Controller)

namespace following_local_planner::ros_plugin {
    using nav2_util::declare_parameter_if_not_declared;

    FollowingRosNode::FollowingRosNode() { current_state = core::State<double>(); };

    void FollowingRosNode::configure(
            const rclcpp_lifecycle::LifecycleNode::WeakPtr &node, std::string name,
            const std::shared_ptr<tf2_ros::Buffer> &tf,
            const std::shared_ptr<nav2_costmap_2d::Costmap2DROS> &costmap_ros) {
        std::string plugin_name;
        this->tf_buffer = tf;
        this->ros_lifecycle_node = node;
        this->cost_map_ros = costmap_ros;
//
//        nav2_util::declare_parameter_if_not_declared(this->ros_lifecycle_node,
//                                                     name + ".plugin_mode",
//                                                     rclcpp::ParameterValue("LQR"));
//        nav2_util::declare_parameter_if_not_declared(this->ros_lifecycle_node,
//                                                     name + ".frequency",
//                                                     rclcpp::ParameterValue(30));
//        nav2_util::declare_parameter_if_not_declared(this->ros_lifecycle_node,
//                                                     name + ".max_speed",
//                                                     rclcpp::ParameterValue(1.));

        if (std::weak_ptr ros_node_weak_ptr = this->ros_lifecycle_node; auto weak_node =
                ros_node_weak_ptr.lock()) {
            weak_node->get_parameter(name + ".plugin_mode", plugin_name);
            weak_node->get_parameter(name + ".frequency", frequency);
            weak_node->get_parameter(name + ".max_speed", max_speed);

            pluginlib::ClassLoader<following_local_planner::core::Follower> poly_loader(
                    "following_local_planner", "following_local_planner::core::Follower");
            try {
                RCLCPP_DEBUG(weak_node->get_logger(), "Trying to init %s",
                             plugin_name.c_str());
                planner = poly_loader.createSharedInstance(plugin_name);

                RCLCPP_INFO(weak_node->get_logger(),
                            "Inited %s Follower Class", plugin_name.c_str());
            } catch (pluginlib::PluginlibException &ex) {
                RCLCPP_ERROR(weak_node->get_logger(),
                             "The plugin failed to load for some reason. Error: %s",
                             ex.what());
            }

            global_vars::setFrequency(frequency);
            global_vars::setMaxSpeed(max_speed);

            global_pub_ = weak_node->create_publisher<nav_msgs::msg::Path>("received_global_plan", 1);
            local_pub_ = weak_node->create_publisher<nav_msgs::msg::Path>("local_plan", 1);
        }
    }

    void FollowingRosNode::activate() {
        global_pub_->on_activate();
        local_pub_->on_activate();
    }

    void FollowingRosNode::deactivate() {
        global_pub_->on_deactivate();
        local_pub_->on_deactivate();
    }

    void FollowingRosNode::cleanup() {
        global_pub_.reset();
        local_pub_.reset();
    }

    void FollowingRosNode::setPlan(const nav_msgs::msg::Path &path) {
        main_loop_mutex.lock();

        nav_msgs::msg::Path biased_path = pathBias(path);

        if (std::weak_ptr ros_node_weak_ptr = this->ros_lifecycle_node; auto weak_node =
                ros_node_weak_ptr.lock()) {
            if (weak_node->count_subscribers("received_global_plan") > 0) {
                global_pub_->publish(biased_path);
            }
        }

        if (std::weak_ptr weak_ptr = planner; auto weak_planner = weak_ptr.lock()) {
            weak_planner->setPlan(biased_path);
        }

        global_path = biased_path;
        main_loop_mutex.unlock();
    }

    nav_msgs::msg::Path FollowingRosNode::pathBias(const nav_msgs::msg::Path &path,
                                                   const double &step) try {
        nav_msgs::msg::Path biased_path;

        biased_path.header = path.header;
        biased_path.poses.reserve(200);
        biased_path.poses.push_back(path.poses[0]);

        int current_size = 0;

        for (size_t i = 0; i < path.poses.size() - 1; ++i) {
            auto iter_first = &path.poses[i];
            auto iter_second = &path.poses[i + 1];

            int count_of_values = std::floor(
                    std::hypot(
                            pow(iter_first->pose.position.x - iter_second->pose.position.x, 2),
                            pow(iter_first->pose.position.y - iter_second->pose.position.y,
                                2)) /
                    step);

            count_of_values = std::isfinite(count_of_values) ? count_of_values : 0;

            double x_steps =
                    fabs(iter_first->pose.position.x - iter_second->pose.position.x) /
                    count_of_values;
            double y_steps =
                    fabs(iter_first->pose.position.y - iter_second->pose.position.y) /
                    count_of_values;

            x_steps = std::isfinite(x_steps) ? x_steps : 0;
            y_steps = std::isfinite(y_steps) ? y_steps : 0;

            /// 0.1 -> steps for path biasing
            /// Simple line iterator
            std::advance(iter_first, current_size);
            std::advance(iter_second, current_size + count_of_values);

            auto getNewPose = [=](auto iter_value) -> auto {
                auto new_value = iter_value;

                new_value.pose.position.x += x_steps;
                new_value.pose.position.y += y_steps;

                return new_value;
            };

            for (int j = current_size; j < current_size + count_of_values; ++j)
                try {
                    biased_path.poses.push_back(getNewPose(biased_path.poses[j - 1]));
                } catch (std::exception &exception) {
                    std::cerr << "FUCK: " << exception.what() << std::endl;
                    continue;
                }
            current_size += count_of_values;
        }

        return biased_path;
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return nav_msgs::msg::Path();
    }

    void FollowingRosNode::updateCurrentState(
            const geometry_msgs::msg::PoseStamped &pose) {
        current_state.x = pose.pose.position.x;
        current_state.y = pose.pose.position.y;

        current_state.yaw = tf2::getYaw(pose.pose.orientation);
    }

    bool FollowingRosNode::mainLoop(geometry_msgs::msg::Twist &velocity) {
        main_loop_mutex.lock();
        if (std::weak_ptr weakPtr = planner; auto weak_planner = weakPtr.lock()) {
            if (!weak_planner->createVelocities(this->global_path, &this->current_state,
                                                velocity)) {
                if (std::weak_ptr ros_node_weak_ptr = this->ros_lifecycle_node; auto weak_node =
                        ros_node_weak_ptr.lock()) {
                    RCLCPP_ERROR(weak_node->get_logger(), "FUCK");
                }
            }
        }
        main_loop_mutex.unlock();

        return true;
    }

    geometry_msgs::msg::TwistStamped FollowingRosNode::computeVelocityCommands(
            const geometry_msgs::msg::PoseStamped &pose,
            const geometry_msgs::msg::Twist &velocity,
            nav2_core::GoalChecker * goal_checker) {
        geometry_msgs::msg::TwistStamped output_velocity;
        geometry_msgs::msg::Twist velocity_(velocity);

        this->updateCurrentState(pose);
        this->mainLoop(velocity_);

        output_velocity.header.frame_id = "map";
        output_velocity.header.stamp = rclcpp::Time();

        output_velocity.twist = velocity_;

        return output_velocity;
    }
} // namespace following_local_planner::ros_plugin
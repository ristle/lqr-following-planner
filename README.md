# LQR local planner

[[_TOC_]]

## Description

This package can be used as controller plugin for [ros2 navigation stack](https://github.com/ros-planning/navigation2/tree/foxy-devel)

For this moment you can use `ILQR` at [this file](src/lqr_plugins/ilqr_plugin.cpp) to build trajectory using robot kinematic model. More you can find follow [this link](https://jonathan-hui.medium.com/rl-lqr-ilqr-linear-quadratic-regulator-a5de5104c750). This is good article about that `Iterative LQR` is about and how it works.  

Also You can use `LQR` with robot model for following the given trajectory using [kinematic and dynamic models](src/lqr_plugins/LQRSystems/LQRDiffNoSpeedModel.cpp). So you implement your own robot model( more read [this](include/following_local_planner/core/LQRFollowingSystem.hpp))

## Write and Load new plugin

### Plugins
All classes: 

- Trajectory Following ( `LQR` )
- Trajectory Creator ( `ILQR` ) 
- Robot model ( `DiffNoSpeedModel` )

you can load dynamically. Read more about [pluginlib](https://docs.ros.org/en/foxy/Tutorials/Pluginlib.html)

### About PluginLib

You can load using pluginlib. Here an example of loading robot model for differential drive without controll of his speed:


```cpp
pluginlib::ClassLoader<core::Model<double>> poly_loader(
        "lqr_system_plugins", "following_local_planner::core::Model<double>");

try {
    model = poly_loader.createSharedInstance("DiffNoSpeedModel");
} catch (pluginlib::PluginlibException &ex) {
    printf("The plugin failed to load for some reason. Error: %s\n", ex.what());
}
```

### Write your plugin 

Write one of the plugins from [section of plugins](#plugins) and create new library in [CMakeLists.txt](./CMakeLists.txt)

```cmake
add_library(ros_navigation_plugin SHARED src/ros_plugin/ros_planner.cpp)
target_include_directories(ros_navigation_plugin PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>)
ament_target_dependencies(ros_navigation_plugin
        rclcpp_lifecycle
        ${dependencies})
```

Choose your plugin type and write a new `xml` file with your plugin and put it in [CMakeLists.txt](./CMakeLists.txt) like code bellow

```cmake

pluginlib_export_plugin_description_file(${PROJECT_NAME} plugins/follower_plugins.xml)
pluginlib_export_plugin_description_file(lqr_system_plugins plugins/lqr_system_plugins.xml)
```

## LQR

We using discrete time for infinite space to follow the given trajectory. We minimizing the given error: **difference between our state and state in which we want to be after some time**

```
auto new_speed = - K * error;
```
How find `K` matrix: 
```latex
K = R * (B^T * X * B)^-1 *( B^T * X * A);
``` 
Where `X` - solution of [Riccati equation](https://en.wikipedia.org/wiki/Algebraic_Riccati_equation)

### LQR plugins

For now I have 2 `LQR` plugins:

- LQR ( without openACC optimization. Class written using `Eigen` )
- LQR_ACC ( with openacc optimization. Class written using `boost::numeric::ublas::matrix<double>` )

Boost speed about 40-50 times ( Without using `Eigen::initParallel ).
With using openmp as backend of Eigen boost is about 7.5-8.5 times. In some cases over 15 times.

Preparation step (build:

```
# source /opt/ros...
cmake -B cmake-build-debug;
cmake --build cmake-build-debug -j 4
```

You can compare it using (`fish shell`):

```fish
set first_middle 0
set second_muddle 0
for i in (seq 1 10)  
      set _a (./cmake-build-debug/lqr_speed_test 2> /dev/null |grep time  )
      set first (echo $_a | awk -F "[:]" '{print $2}' |awk '{print $1}')
      set second (echo $_a | awk -F "[:]" '{print $3}' |awk '{print $1}')

      set first_middle (math $first_middle + $first)
      set second_muddle (math $second_muddle + $second)
end; 
set_color green
printf "Speed time: "
set_color red
math (math $first_middle / 10) / (math $second_muddle / 10)
set_color green
printf "times\n"
set_color normal
```

## Configuration file

He placed in [plugin directory](./plugins/follower_local_planner.yaml)

## Architecture

### Ros Plugin loading steps

```mermaid
graph TD
    A[FollowingRosNode] -->|Dynamic Loading| B(Plugins)
    B --> C{Choose Following Node}
    C -->|Without Optimiation| D[LQR]
    C -->|With Optimization| E[LQR_ACC]
    B --> DD{Choose Trajectory Node}
    DD -->|Non Stable method| LQR[Iterative LQR]
    DD -->|Stable Global Path biaing| None[None]

```

### Following Node Classes

```mermaid
graph TD
    A[following_local_planner::core::Follower] -->|Abstract Class| B(Realizations)
    B -->|Without Optimiation| D[LQR]
    B -->|With Optimization| E[LQR_ACC]
    
    Method -->|Required Methods| R[Overide] 
    R -->|Create speed in main loop| getSpeed

    Method -->|Optional Methods| O[Overide]
    O -->|You simple override this function and do what ever you want| additional
```


### Trajectory Node Classes

Just go to Doxygen Docs [at this file](./include/following_local_planner/core/Trajectory.hpp)

//
// Created by ristle on 4/11/22.
//

#include "matplotlib.hpp"
#include <bits/stdc++.h>
#include <following_local_planner/lqr_plugins/ilqr_plugin.hpp>

#include <rclcpp/rclcpp.hpp>
#include "matplotlib.hpp"
#include "gtest/gtest.h"
#include "following_local_planner/lqr_plugins/lqr_acc_planner.hpp"
#include <bits/stdc++.h>

#ifdef RMW_IMPLEMENTATION
#define CLASSNAME_(NAME, SUFFIX) NAME##__##SUFFIX
#define CLASSNAME(NAME, SUFFIX) CLASSNAME_(NAME, SUFFIX)
#else
#define CLASSNAME(NAME, SUFFIX) NAME
#endif

namespace plt = matplotlibcpp;


class ScopedTimer : public std::enable_shared_from_this<ScopedTimer> {
public:
    ScopedTimer() {
        start = clock();
        /// Just to turn off warning
        end = clock();
    }

    ~ScopedTimer() {
        end = clock();
        printf("time:  %0.8f sec\n",
               (float(end - start)) / CLOCKS_PER_SEC);
    }

private:
    clock_t start;
    clock_t end;
};

class CLASSNAME(ilqr_speed_test, RMW_IMPLEMENTATION) : public ::testing::Test {
public:
    static void SetUpTestCase() { rclcpp::init(0, nullptr); }

    static void TearDownTestCase() { rclcpp::shutdown(); }

protected:
    int frequency{30};
    float dt = 1. / frequency;

    nav_msgs::msg::Path fill_sample_path(const std::vector<std::pair<double, double>> &input_values) {
        nav_msgs::msg::Path output_path;

        for (const auto &iter: input_values) {
            output_path.poses.push_back(
                    geometry_msgs::msg::PoseStamped().set__pose(
                            geometry_msgs::msg::Pose().set__position(
                                    geometry_msgs::msg::Point().set__x(iter.first)
                                            .set__y(iter.second)
                            )
                    )
            );
        }
        return output_path;
    }

    void process_path(std::shared_ptr<following_local_planner::core::Follower> &lqr,
                      bool update_state = true) {
        auto model = std::make_shared<following_local_planner::plugins::DiffNoSpeedModel<double>>();
        auto path = fill_sample_path({
                                             {1.,  0},
                                             {1.1, 0.},
                                             {1.2, 0},
                                             {1.3, 0},
                                             {1.4, 0},
                                             {1.5, 0},
                                             {1.6, 0},
                                             {1.7, 0},
                                             {1.8, 0},
                                             {1.9, 0},
                                             {2.,  0},
                                             {2.1, 0},
                                             {2.2, 0},
                                             {2.3, 0},
                                             {2.4, 0},
                                             {2.5, 0},
                                             {2.6, 0},
                                             {2.7, 0},
                                             {2.8, 0},
                                             {2.9, 0},
                                             {3.,  0}
                                     });
        auto biased_path = path;
        following_local_planner::core::State<double> state(1, 0.1);
        state.yaw = 0.0;

        lqr->setPlan(biased_path);
        model->setTrajectory(&biased_path, &biased_path);
        std::vector<double> x, y;
        std::vector<double> x_path, y_path;

        x.reserve(1000);
        y.reserve(1000);

        x_path.reserve(200);
        y_path.reserve(200);

        for (auto pose: biased_path.poses) {
            x_path.push_back(pose.pose.position.x);
            y_path.push_back(pose.pose.position.y);
        }

        {
            auto timer = std::make_shared<ScopedTimer>();

            x.push_back(state.x);
            y.push_back(state.y);
            for (int i = 0; i < 80; ++i) {
                double distance_to_end = std::hypot(state.x - x_path[x_path.size() - 1],
                                                    state.y - y_path[y_path.size() - 1]);
                if (distance_to_end < 0.51) {
                    break;
                }

                auto speed = lqr->getSpeed(&state);
                model->setState(&state);

                for (int j = 0; j < speed.rows(); ++j) {
                    if (!isfinite(speed(j))) {
                        speed(j) = 0;
                    }
                }

                Eigen::VectorXd current_pose(3);
                current_pose << state.x, state.y, state.yaw;

                auto new_pose = model->integrateDynamics(current_pose, speed, dt);

                if (update_state) {
                    state.x = new_pose(0);
                    state.y = new_pose(1);
                    state.yaw = fmod(new_pose(2), M_PI);
                }

                x.push_back(state.x);
                y.push_back(state.y);
            }
        }
        if (update_state) {
            plt::figure(); // declare a new figure (optional if only one is used)

            plt::plot(x, y);                        // automatic coloring: tab:blue
            plt::scatter(x_path, y_path);                        // automatic coloring: tab:blue

            plt::title("Standard usage"); // set a title
            plt::show();
        }
    }
};

TEST_F(CLASSNAME(ilqr_speed_test, RMW_IMPLEMENTATION), lqr_with_acc) {
    std::shared_ptr<following_local_planner::core::Follower> lqr_acc =
            std::make_shared<following_local_planner::plugins::LQR_ACC<double>>();

    std::shared_ptr<following_local_planner::core::Trajectory> ilqr =
            std::make_shared<following_local_planner::plugins::iLQR>();

    following_local_planner::core::State<double> state(1, 0.0);
    state.yaw = 0.0;

    ilqr->setState(&state);

    std::cout << "\niLqr Without OpenACC ";
    auto path = fill_sample_path({
                                         {10.,  0.0},
                                         {12.,  0.0},
                                         {15.,  0.}
                                 });
    ilqr->createTrajectory(path);

    nav_msgs::msg::Path trajectory;
    trajectory = *ilqr->getTrajectory();

    std::vector<double> x, y, w;
    int size_ = trajectory.poses.size();
    for (const auto &iter: trajectory.poses) {
        x.push_back(iter.pose.position.x);
        y.push_back(iter.pose.position.y);
        w.push_back(iter.pose.position.z);
    }

    plt::figure();
    plt::plot(x, y);

    plt::title("iLQR"); // set a title
    plt::show();

    std::cout << "all okay\n";
}
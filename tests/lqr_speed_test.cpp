//
// Created by ristle on 3/19/22.
//

/// This file is created only for Comparing speed if LQR with Openacc and without it

#include <following_local_planner/lqr_plugins/LQRSystems/LQRDiffNoSpeedModel.hpp>
#include <following_local_planner/lqr_plugins/lqr_acc_planner.hpp>
#include <following_local_planner/lqr_plugins/lqr_planner.hpp>
#include <following_local_planner/ros_plugin/ros_planner.hpp>

#include <rclcpp/rclcpp.hpp>
#include "matplotlib.hpp"
#include "gtest/gtest.h"
#include <bits/stdc++.h>

#ifdef RMW_IMPLEMENTATION
#define CLASSNAME_(NAME, SUFFIX) NAME##__##SUFFIX
#define CLASSNAME(NAME, SUFFIX) CLASSNAME_(NAME, SUFFIX)
#else
#define CLASSNAME(NAME, SUFFIX) NAME
#endif

namespace plt = matplotlibcpp;

class ScopedTimer : public std::enable_shared_from_this<ScopedTimer> {
public:
    ScopedTimer() {
        start = clock();
        /// Just to turn off warning
        end = clock();
    }

    ~ScopedTimer() {
        end = clock();
        printf("time:  %0.8f sec\n",
               (float(end - start)) / CLOCKS_PER_SEC);
    }

private:
    clock_t start;
    clock_t end;
};

class CLASSNAME(lqr_speed_test, RMW_IMPLEMENTATION) : public ::testing::Test {
public:
    static void SetUpTestCase() { rclcpp::init(0, nullptr); }

    static void TearDownTestCase() { rclcpp::shutdown(); }

protected:
    int frequency{30};
    float dt = 1. / frequency;

    nav_msgs::msg::Path fill_sample_path(const std::vector<std::pair<double, double>> &input_values) {
        nav_msgs::msg::Path output_path;

        for (const auto &iter: input_values) {
            output_path.poses.push_back(
                    geometry_msgs::msg::PoseStamped().set__pose(
                            geometry_msgs::msg::Pose().set__position(
                                    geometry_msgs::msg::Point().set__x(iter.first)
                                            .set__y(iter.second)
                            )
                    )
            );
        }
        return output_path;
    }

    void run_algorithm(std::shared_ptr<following_local_planner::core::Follower> &lqr,
                       nav_msgs::msg::Path &biased_path,
                       following_local_planner::core::State<double> state,
                       bool update_state = true) {
        auto model = std::make_shared<following_local_planner::plugins::DiffNoSpeedModel<double>>();

        lqr->setPlan(biased_path);
        model->setTrajectory(&biased_path, &biased_path);
        std::vector<double> x, y;
        std::vector<double> x_path, y_path;

        x.reserve(1000);
        y.reserve(1000);

        x_path.reserve(200);
        y_path.reserve(200);

        for (auto pose: biased_path.poses) {
            x_path.push_back(pose.pose.position.x);
            y_path.push_back(pose.pose.position.y);
        }

        {
            auto timer = std::make_shared<ScopedTimer>();

            x.push_back(state.x);
            y.push_back(state.y);
            for (int i = 0; i < 80; ++i) {
                double distance_to_end = std::hypot(state.x - x_path[x_path.size() - 1],
                                                    state.y - y_path[y_path.size() - 1]);
                if (distance_to_end < 0.51) {
                    break;
                }

                auto speed = lqr->getSpeed(&state);
                model->setState(&state);

                for (int j = 0; j < speed.rows(); ++j) {
                    if (!isfinite(speed(j))) {
                        speed(j) = 0;
                    }
                }

                Eigen::VectorXd current_pose(3);
                current_pose << state.x, state.y, state.yaw;

                auto new_pose = model->integrateDynamics(current_pose, speed, dt);

                if (update_state) {
                    state.x = new_pose(0);
                    state.y = new_pose(1);
                    state.yaw = fmod(new_pose(2), M_PI);
                }

                x.push_back(state.x);
                y.push_back(state.y);
            }
        }
        if (update_state) {
            plt::figure(); // declare a new figure (optional if only one is used)

            plt::named_plot("Пройденный путь", x, y);                        // automatic coloring: tab:blue
            plt::named_plot("Желаемый путь", x_path, y_path);                        // automatic coloring: tab:blue

            plt::title("LQR"); // set a title
            plt::legend();
            plt::show();
        }
    }

    void process_path(std::shared_ptr<following_local_planner::core::Follower> &lqr,
                      bool update_state = true) {
        auto path = fill_sample_path({
                                             {1.,  0},
                                             {1.1, 0.},
                                             {1.2, 0},
                                             {1.3, 0},
                                             {1.4, 0},
                                             {1.5, 0},
                                             {1.6, 0},
                                             {1.7, 0},
                                             {1.8, 0},
                                             {1.9, 0},
                                             {2.,  0},
                                             {2.1, 0},
                                             {2.2, 0},
                                             {2.3, 0},
                                             {2.4, 0},
                                             {2.5, 0},
                                             {2.6, 0},
                                             {2.7, 0},
                                             {2.8, 0},
                                             {2.9, 0},
                                             {3.,  0}
                                     });
        following_local_planner::core::State<double> state(1, 0.1);
        state.yaw = 0.0;
        run_algorithm(lqr, path, state);
    }
};


TEST_F(CLASSNAME(lqr_speed_test, RMW_IMPLEMENTATION), lqr_without_acc) {
    std::shared_ptr<following_local_planner::core::Follower> lqr =
            std::make_shared<following_local_planner::plugins::LQR<double>>();

    std::cout << "\nLqr With Eigen(with OpenMP) ";
    process_path(lqr);
}


TEST_F(CLASSNAME(lqr_speed_test, RMW_IMPLEMENTATION), lqr_with_acc) {
    std::shared_ptr<following_local_planner::core::Follower> lqr_acc =
            std::make_shared<following_local_planner::plugins::LQR_ACC<double>>();

    std::cout << "\nLqr With OpenACC ";
    process_path(lqr_acc);
    plt::close();
}

TEST_F(CLASSNAME(lqr_speed_test, RMW_IMPLEMENTATION), lqr_with_acc_2) {
    std::shared_ptr<following_local_planner::core::Follower> lqr_acc =
            std::make_shared<following_local_planner::plugins::LQR_ACC<double>>();

    std::cout << "\nLqr With OpenACC ";
    auto path = fill_sample_path({
                                         {1.,  0},
                                         {1.1, 0.},
                                         {1.2, 0},
                                         {1.3, 0},
                                         {1.4, 0},
                                         {1.5, 0},
                                         {1.6, 0},
                                         {1.7, 0},
                                         {1.8, 0},
                                         {1.9, 0},
                                         {2.,  0},
                                         {2.1, 0},
                                         {2.2, 0},
                                         {2.3, 0},
                                         {2.4, 0},
                                         {2.5, 0},
                                         {2.6, 0},
                                         {2.7, 0},
                                         {2.8, 0},
                                         {2.9, 0},
                                         {3.,  0}
                                 });
    following_local_planner::core::State<double> state(0., 0.05);
    state.yaw = 0.0;
    run_algorithm(lqr_acc, path, state);

    plt::close();
}

TEST_F(CLASSNAME(lqr_speed_test, RMW_IMPLEMENTATION), matrix_multiplication) {
    std::shared_ptr<following_local_planner::plugins::LQR_ACC<double>>
            lqr = std::make_shared<following_local_planner::plugins::LQR_ACC<double>>();

    using namespace boost::numeric::ublas;
    matrix<double> a = zero_matrix<double>(3, 3);
    matrix<double> b = zero_matrix<double>(3, 3);
    matrix<double> c = zero_matrix<double>(3, 3);
    matrix<double> output_matrix;

    for (size_t i = 0; i < a.size1(); ++i) {
        for (size_t j = 0; j < a.size2(); ++j) {
            a(i, j) = i + j;
            b(i, j) = 2;
            c(i, j) = 1.5;
        }
    }
    lqr->reduction_matrix(&a, &b, &output_matrix);

    for (size_t i = 0; i < a.size1(); ++i) {
        for (size_t j = 0; j < a.size2(); ++j) {
            ASSERT_DOUBLE_EQ(output_matrix(i, j), i + j + 2);
        }
    }

    lqr->minus_matrix(&a, &b, &output_matrix);

    for (int i = 0; i < (int) a.size1(); ++i) {
        for (int j = 0; j < (int) a.size2(); ++j) {
            ASSERT_DOUBLE_EQ(output_matrix(i, j), i + j - 2);
        }
    }

    lqr->multiply_matrix(&a, &b, &output_matrix);
    Eigen::MatrixXd expected_result = lqr->get_eigen_matrix_from_boost(a) *
                                      (lqr->get_eigen_matrix_from_boost(b));

    for (size_t i = 0; i < a.size1(); ++i) {
        for (size_t j = 0; j < a.size2(); ++j) {
            ASSERT_DOUBLE_EQ(output_matrix(i, j), expected_result(i, j));
        }
    }

    lqr->multiply_matrix(&a, &b, &c, &output_matrix);

    Eigen::MatrixXd expected_result_second = expected_result *
                                             (lqr->get_eigen_matrix_from_boost(c));

    for (size_t i = 0; i < a.size1(); ++i) {
        for (size_t j = 0; j < a.size2(); ++j) {
            ASSERT_DOUBLE_EQ(output_matrix(i, j), expected_result_second(i, j));
        }
    }
    bool success{true};
    boost::numeric::ublas::matrix<double> inverse_boost_matrix;
    following_local_planner::functional::get_inverse<>(&a, &inverse_boost_matrix);

    Eigen::MatrixXd inverse_matrix = lqr->get_eigen_matrix_from_boost(a).inverse();

    /* initialize the values in matrix A */
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (j < i) {
                c(i, j) = 0;
            } else {
                c(i, j) = j - i + 1;
            }
        }
    }

    double result{0.0};
    result = following_local_planner::functional::get_determinant<double>(&c);
    ASSERT_DOUBLE_EQ(result, 1.);

    auto inverse_matrix_boost = new boost::numeric::ublas::matrix<double>(3, 3);
    following_local_planner::functional::get_inverse<>(&c, inverse_matrix_boost);

    std::cout << "FUCKERD UP !\n";
    for (int i = 0; i < inverse_matrix_boost->size1(); i++) {
        for (int j = 0; j < inverse_matrix_boost->size2(); j++) {
            std::cout << std::setw(8) << (*inverse_matrix_boost)(i, j) << " ";
        }
        std::cout << std::endl;
    }
}
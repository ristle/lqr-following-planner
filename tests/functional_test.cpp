//
// Created by ristle on 2/19/22.
//

#include "gtest/gtest.h"
#include <bits/stdc++.h>

#include "rclcpp/rclcpp.hpp"
#include <following_local_planner/ros_plugin/ros_planner.hpp>

#ifdef RMW_IMPLEMENTATION
#define CLASSNAME_(NAME, SUFFIX) NAME##__##SUFFIX
#define CLASSNAME(NAME, SUFFIX) CLASSNAME_(NAME, SUFFIX)
#else
#define CLASSNAME(NAME, SUFFIX) NAME
#endif

class CLASSNAME(functional_test, RMW_IMPLEMENTATION) : public ::testing::Test {
public:
  static void SetUpTestCase() { rclcpp::init(0, nullptr); }

  static void TearDownTestCase() { rclcpp::shutdown(); }
};

TEST_F(CLASSNAME(functional_test, RMW_IMPLEMENTATION), path_bias_test) {
  auto node = rclcpp::Node::make_shared("pathBiasTest");
  nav_msgs::msg::Path path;

  auto createPoseStamped = [&](double x, double y,
                               double w) -> geometry_msgs::msg::PoseStamped {
    geometry_msgs::msg::PoseStamped return_value;

    return_value.pose.position.x = x;
    return_value.pose.position.y = y;
    return_value.pose.position.z = 0;

    return_value.pose.orientation.z = w;

    return return_value;
  };

  path.header.stamp = rclcpp::Time();
  path.header.frame_id = "map";

  for (int i = 1; i < 5; ++i) {
    path.poses.push_back(createPoseStamped(i * 2., i * 2., 1));
    RCLCPP_INFO(node.get()->get_logger(), "index: %i x: %f y: %f ", i, i * 2.,
                i * 2.);
  }
  auto biased_path =
      following_local_planner::ros_plugin::FollowingRosNode::pathBias(path,
                                                                      0.18);

  for (auto &iter : biased_path.poses) {
    RCLCPP_INFO(node.get()->get_logger(), " x: %f y: %f ", iter.pose.position.x,
                iter.pose.position.y);
  }
};